import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HostTriviaComponent } from './host-trivia.component';

describe('HostTriviaComponent', () => {
  let component: HostTriviaComponent;
  let fixture: ComponentFixture<HostTriviaComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [HostTriviaComponent]
    });
    fixture = TestBed.createComponent(HostTriviaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
