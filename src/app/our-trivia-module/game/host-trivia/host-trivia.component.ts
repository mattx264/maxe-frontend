import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-host-trivia',
  templateUrl: './host-trivia.component.html',
  styleUrls: ['./host-trivia.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HostTriviaComponent {

}
