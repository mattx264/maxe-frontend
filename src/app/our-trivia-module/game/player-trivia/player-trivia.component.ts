import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-player-trivia',
  templateUrl: './player-trivia.component.html',
  styleUrls: ['./player-trivia.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlayerTriviaComponent {

}
