import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerTriviaComponent } from './player-trivia.component';

describe('PlayerTriviaComponent', () => {
  let component: PlayerTriviaComponent;
  let fixture: ComponentFixture<PlayerTriviaComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PlayerTriviaComponent]
    });
    fixture = TestBed.createComponent(PlayerTriviaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
