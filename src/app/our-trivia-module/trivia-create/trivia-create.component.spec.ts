import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TriviaCreateComponent } from './trivia-create.component';

describe('TriviaCreateComponent', () => {
  let component: TriviaCreateComponent;
  let fixture: ComponentFixture<TriviaCreateComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TriviaCreateComponent]
    });
    fixture = TestBed.createComponent(TriviaCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
