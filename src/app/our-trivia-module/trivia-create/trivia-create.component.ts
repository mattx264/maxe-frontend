import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-trivia-create',
  templateUrl: './trivia-create.component.html',
  styleUrls: ['./trivia-create.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TriviaCreateComponent {

}
