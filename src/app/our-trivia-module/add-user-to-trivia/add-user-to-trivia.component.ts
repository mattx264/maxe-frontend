import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-add-user-to-trivia',
  templateUrl: './add-user-to-trivia.component.html',
  styleUrls: ['./add-user-to-trivia.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddUserToTriviaComponent {

}
