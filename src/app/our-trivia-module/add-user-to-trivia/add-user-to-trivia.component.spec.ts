import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddUserToTriviaComponent } from './add-user-to-trivia.component';

describe('AddUserToTriviaComponent', () => {
  let component: AddUserToTriviaComponent;
  let fixture: ComponentFixture<AddUserToTriviaComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AddUserToTriviaComponent]
    });
    fixture = TestBed.createComponent(AddUserToTriviaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
