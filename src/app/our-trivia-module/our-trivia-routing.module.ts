import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TriviaCreateComponent } from './trivia-create/trivia-create.component';
import { AddUserToTriviaComponent } from './add-user-to-trivia/add-user-to-trivia.component';
import { PlayerTriviaComponent } from './game/player-trivia/player-trivia.component';
import { HostTriviaComponent } from './game/host-trivia/host-trivia.component';

const routes: Routes = [
  {
    path: 'create-trivia',
    component: TriviaCreateComponent,
  },
  {
    path: 'add-user',
    component: AddUserToTriviaComponent,
  },
  {
    path: 'player-trivia',
    component: PlayerTriviaComponent,
  },
  {
    path: 'host',
    component: HostTriviaComponent,
  },
  {
    path: '**',
    redirectTo: 'create-trivia',
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OurTriviaRoutingModule {}
