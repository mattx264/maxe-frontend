import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TriviaCreateComponent } from './trivia-create/trivia-create.component';
import { OurTriviaRoutingModule } from './our-trivia-routing.module';
import { AddUserToTriviaComponent } from './add-user-to-trivia/add-user-to-trivia.component';
import { HostTriviaComponent } from './game/host-trivia/host-trivia.component';
import { PlayerTriviaComponent } from './game/player-trivia/player-trivia.component';

@NgModule({
  imports: [
    CommonModule,
    OurTriviaRoutingModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [TriviaCreateComponent, AddUserToTriviaComponent, HostTriviaComponent, PlayerTriviaComponent],
  providers: [],
})
export class OurTriviaModule {}
