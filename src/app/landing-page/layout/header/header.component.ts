import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { UserViewModel } from 'src/app/api-client';
import { AuthService } from 'src/app/auth/services/auth.service';
import { selectUser } from 'src/app/store/user.selectors';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  user: UserViewModel;

  constructor(private authService: AuthService, private store: Store) {}

  ngOnInit() {
    this.store.pipe(select(selectUser)).subscribe((user) => {
      this.user = user;
    });
  }
  logoutClick() {
    this.authService.logout();
  }
}
