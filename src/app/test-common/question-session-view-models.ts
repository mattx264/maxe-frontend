import {
  AnswerType,
  AnswerViewModel,
  ContentSectionAnswerType,
  ContentSectionQuestionType,
  QuestionSectionViewModel,
  QuestionSessionViewModel,
} from '../api-client';

export class QuestionSessionViewModelQA {
  static getFILL_BLANK() {
    //todo test data has to be update to have valid fill blank dataS
    return [
      new QuestionSessionViewModel({
        questionId: 3,
        questionSections: [
          new QuestionSectionViewModel({
            type: ContentSectionQuestionType.TEXT,
            value: 'q3',
          }),
        ],
        answers: [
          new AnswerViewModel({
            value: 'a1',
            type: ContentSectionAnswerType.FILL_BLANK,
          })
        ],
        isQuestionPlayed: false,
        questionType: AnswerType.FILL_BLANK,
      }),
    ];
  }
  static getQA(): QuestionSessionViewModel[] {
    return [
      new QuestionSessionViewModel({
        questionId: 1,
        questionSections: [
          new QuestionSectionViewModel({
            type: ContentSectionQuestionType.TEXT,
            value: 'q1',
          }),
        ],
        answers: [
          new AnswerViewModel({
            value: 'a1',
            type: ContentSectionAnswerType.TEXT,
          }),
        ],
        isQuestionPlayed: false,
        questionType: AnswerType.QUIZ,
      }),
      new QuestionSessionViewModel({
        questionId: 2,
        questionSections: [
          new QuestionSectionViewModel({
            type: ContentSectionQuestionType.TEXT,
            value: 'q2',
          }),
        ],
        answers: [
          new AnswerViewModel({
            value: 'a1',
            type: ContentSectionAnswerType.TEXT,
          }),
          new AnswerViewModel({
            value: 'a2',
            type: ContentSectionAnswerType.TEXT,
          }),
        ],
        isQuestionPlayed: false,
        questionType: AnswerType.QUIZ,
      }),
      new QuestionSessionViewModel({
        questionId: 3,
        questionSections: [
          new QuestionSectionViewModel({
            type: ContentSectionQuestionType.TEXT,
            value: 'q3',
          }),
        ],
        answers: [
          new AnswerViewModel({
            value: 'a1',
            type: ContentSectionAnswerType.TEXT,
          }),
          new AnswerViewModel({
            value: 'a2',
            type: ContentSectionAnswerType.TEXT,
          }),
          new AnswerViewModel({
            value: 'a3',
            type: ContentSectionAnswerType.TEXT,
          }),
        ],
        isQuestionPlayed: false,
        questionType: AnswerType.QUIZ,
      }),
      new QuestionSessionViewModel({
        questionId: 4,
        questionSections: [
          new QuestionSectionViewModel({
            type: ContentSectionQuestionType.TEXT,
            value: 'q4',
          }),
        ],
        answers: [
          new AnswerViewModel({
            value: 'a1',
            type: ContentSectionAnswerType.TEXT,
          }),
          new AnswerViewModel({
            value: 'a2',
            type: ContentSectionAnswerType.TEXT,
          }),
          new AnswerViewModel({
            value: 'a3',
            type: ContentSectionAnswerType.TEXT,
          }),
          new AnswerViewModel({
            value: 'a4',
            type: ContentSectionAnswerType.TEXT,
          }),
        ],
        isQuestionPlayed: false,
        questionType: AnswerType.QUIZ,
      }),
      new QuestionSessionViewModel({
        questionId: 5,
        questionSections: [
          new QuestionSectionViewModel({
            type: ContentSectionQuestionType.TEXT,
            value: 'q5',
          }),
        ],
        answers: [
          new AnswerViewModel({
            value: 'a1',
            type: ContentSectionAnswerType.TEXT,
          }),
        ],
        isQuestionPlayed: false,
        questionType: AnswerType.QUIZ,
      }),
    ];
  }
  static getQUIZ(): QuestionSessionViewModel[] {
    return [
      new QuestionSessionViewModel({
        questionId: 3,
        questionSections: [
          new QuestionSectionViewModel({
            type: ContentSectionQuestionType.TEXT,
            value: 'q3',
          }),
        ],
        answers: [
          new AnswerViewModel({
            value: 'a1',
            type: ContentSectionAnswerType.TEXT,
          }),
          new AnswerViewModel({
            value: 'a2',
            type: ContentSectionAnswerType.TEXT,
          }),
          new AnswerViewModel({
            value: 'a3',
            type: ContentSectionAnswerType.TEXT,
          }),
        ],
        isQuestionPlayed: false,
        questionType: AnswerType.QUIZ,
      }),
    ];
  }
  static getList(): QuestionSessionViewModel[] {
    return [
      new QuestionSessionViewModel({
        questionId: 3,
        questionSections: [
          new QuestionSectionViewModel({
            type: ContentSectionQuestionType.TEXT,
            value: 'q3',
          }),
        ],
        answers: [
          new AnswerViewModel({
            value: 'a1',
            type: ContentSectionAnswerType.TEXT,
          }),
          new AnswerViewModel({
            value: 'a2',
            type: ContentSectionAnswerType.TEXT,
          }),
          new AnswerViewModel({
            value: 'a3',
            type: ContentSectionAnswerType.TEXT,
          }),
        ],
        isQuestionPlayed: false,
        questionType: AnswerType.LIST_DND,
      }),
    ];
  }
  static getSVG(): QuestionSessionViewModel[] {
    return [
      new QuestionSessionViewModel({
        questionId: 3,
        questionSections: [
          new QuestionSectionViewModel({
            type: ContentSectionQuestionType.TEXT,
            value: 'q3',
          }),
        ],
        answers: [
          new AnswerViewModel({
            value: 'a1',
            type: ContentSectionAnswerType.TEXT,
          }),
          new AnswerViewModel({
            value: 'a2',
            type: ContentSectionAnswerType.TEXT,
          }),
          new AnswerViewModel({
            value: 'a3',
            type: ContentSectionAnswerType.TEXT,
          }),
        ],
        isQuestionPlayed: false,
        questionType: AnswerType.QUIZ,
      }),
    ];
  }
}
