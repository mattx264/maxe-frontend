import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth/services/auth.service';
import { Test } from 'src/app/models/test';
import { HttpClientService } from 'src/app/shared/services/http-client.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  tests: Test[] = [];
  constructor( private authService: AuthService, private httpClient: HttpClientService) { }

  ngOnInit() {
    this.httpClient.get('test').subscribe((response:any[])=>{
      this.tests = response;
    });
    //this.db.collection('tests', ref => ref.where('userId', '==', this.authService.getCurrentUser().uid)).valueChanges().subscribe((data: Test[]) => {
   //   this.tests = data;
   // });
  }
}
