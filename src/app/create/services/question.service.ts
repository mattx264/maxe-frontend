import { Injectable } from '@angular/core';
import { HttpClientService } from 'src/app/shared/services/http-client.service';
import { QuestionUpsertViewModel } from '../models/question-upsert-view-model';
import { Route } from '@angular/compiler/src/core';
import { ActivatedRoute } from '@angular/router';
import { CreateTestViewModel } from '../models/create-test-view-model';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  constructor(private httpClient: HttpClientService) { }

  private _testId: number;
  public get testId(): number {
    if (this._testId == null) {
      throw new Error("Test id is not init");
    }
    return this._testId;
  }
  public set testId(v: number) {
    this._testId = v;
  }

  private _createTestViewModel: CreateTestViewModel;
  public get createTestViewModel(): CreateTestViewModel {
    return this._createTestViewModel;
  }
  public set createTestViewModel(v: CreateTestViewModel) {
    this._createTestViewModel = v;
  }

  getAllQuestions(): Promise<CreateTestViewModel> {
    return new Promise((resolve, reject) => {
      this.httpClient.get('test/' + this.testId).subscribe((result: CreateTestViewModel) => {
        this.createTestViewModel = result;
        resolve(result);
      });
    });

  }
  getQuestionByIndex(index: number): Promise<QuestionUpsertViewModel> {
    return new Promise((resolve, reject) => {
      if (this.createTestViewModel == null) {
        this.getAllQuestions().then(result => {
          resolve(result.questions[index]);
        });
        return;
      }
      resolve(this.createTestViewModel.questions[index]);

    });

  }
  sendQuestion(questionForm: any, questionTypeId: number, questionId?: number): Promise<void> {
    return new Promise((resolve, reject) => {
      const questionViewMode: QuestionUpsertViewModel = {
        id: questionId,
        answer: JSON.stringify(questionForm.get('answers').value),
        questionText: JSON.stringify(questionForm.get('question').value),
        questionTypeId: questionTypeId,
        testtId: this.testId
      }
      if (questionId == null) {
        this.httpClient.post('TestPost/Question', questionViewMode).subscribe(result => {
          resolve()
        }, () => reject());
      } else {
        this.httpClient.put('TestPost/Question', questionViewMode).subscribe(result => {
          resolve()
        }, () => reject());
      }
    });

  }
}
