import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { NestedTreeControl } from '@angular/cdk/tree';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { Router } from '@angular/router';

@Component({
  selector: 'app-setup-page',
  templateUrl: './setup-page.component.html',
  styleUrls: ['./setup-page.component.scss'],
})
export class SetupPageComponent implements OnInit {
  formGroup: FormGroup;
  testCategorysIds: TestCategoryViewModel[];
  // nestedTreeControl: NestedTreeControl<TestCategoryViewModel>;
  // nestedDataSource: MatTreeNestedDataSource<TestCategoryViewModel>;

  constructor(
    private fb: FormBuilder,
    private httpClient: HttpClientService,
    private router: Router
  ) {}

  ngOnInit() {
    this.formGroup = this.buildForm();
    //this.nestedTreeControl = new NestedTreeControl<TestCategoryViewModel>(node => node.subTestCategories);
    //this.nestedDataSource = new MatTreeNestedDataSource<TestCategoryViewModel>();

    this.httpClient.get('testCategory').subscribe((next) => {
      this.testCategorysIds = JSON.parse(JSON.stringify(next));
      //  this.nestedDataSource.data = this.testCategorysIds;
    });
  }
  buildForm(): FormGroup {
    return this.fb.group({
      name: [null, Validators.required],
      description: [null, Validators.required],
      isPublic: [true],
      testCategoryId: [null, Validators.required],
    });
  }
  // addToList(id) {
  //   if (this.testCategorysIds.includes(id)) {
  //     this.testCategorysIds.splice(this.testCategorysIds.indexOf(id), 1);
  //   } else {
  //     this.testCategorysIds.push(id);
  //   }
  //   this.formGroup.get('testCategoryId').setValue(this.testCategorysIds);

  // }
  setupClicked() {
    if (this.formGroup.invalid) {
      return;
    }
    this.httpClient
      .post('TestPost/Setup', this.formGroup.getRawValue())
      .subscribe((result: any) => {
        this.router.navigate(['/create/question', result.id, 1]);
      });
  }
}
