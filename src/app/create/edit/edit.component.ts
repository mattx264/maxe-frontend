import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { AuthService } from 'src/app/auth/services/auth.service';
import { Test } from 'src/app/models/test';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClientService } from 'src/app/shared/services/http-client.service';
import { TestViewModel } from 'src/app/viewModels/test-view-model';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
})
export class EditComponent implements OnInit, OnDestroy {
  test: Test;
  sub: any;
  formGroup: FormGroup;
  questionIndex: number;
  quizType: any;
  questionModel: any;
  submitData: any;
  initInfoShow = true; // this value is true -> for testing you can set it for false

  reviewShow = false;
  /** EDIT VAR **/
  testId: number;
  /** END EDIT VAR **/
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private httpClient: HttpClientService,
    private authService: AuthService,
    private fb: FormBuilder
  ) {}
  ngOnInit() {
    this.sub = this.activatedRoute.params.subscribe((params) => {
      this.testId = params['examId'];
      if (this.testId === undefined) {
        this.router.navigate(['/create/list']);
      }
      this.httpClient.get('test/' + this.testId).subscribe((response: any) => {
        this.test = response;
        this.formGroup = this.fb.group({
          description: this.test.description,
          isPublic: this.test.isPublic,
          name: this.test.name,
          testCategoryId: this.test.testCategoryId,
        });
      });
    });
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  setQuestion(index: number) {
    this.questionIndex = index;
    this.quizType = this.test[index].questionType;
    this.questionModel = this.test[index].formGroup;
  }
  questionsDone(data) {
    this.submitData.questions = data;
    this.reviewShow = true;
  }
  submitClicked() {
    if (this.initInfoShow === true) {
      this.initInfoShow = false;
      this.submitData = this.formGroup.getRawValue();
    } else {
    }
  }
  saveTestClick() {
    //
    let testVM: TestViewModel = {
      id: this.testId,
      name: this.submitData.name,
      description: this.submitData.description,
      isPublic: this.submitData.isPublic,
      testCategoryId: this.submitData.testCategoryId,
      questions: [],
    };
    for (let i = 0; i < this.submitData.questions.length; i++) {
      const element = this.submitData.questions[i];
      testVM.questions.push({
        id: element.id,
        questionTypeId: element.questionTypeId,
        questionText: element.questionText,
        answer: JSON.stringify(element.answer), // JSON
      });
    }
    this.httpClient.put('test', testVM).subscribe((result) => {
      this.router.navigate(['/create/list']);
    });
  }
}
