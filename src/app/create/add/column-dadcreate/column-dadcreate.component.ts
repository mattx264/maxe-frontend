import { Component, OnInit } from '@angular/core';
import { CreateComponentBase } from '../createComponentBase';
import { UntypedFormBuilder, UntypedFormGroup, Validators, UntypedFormArray } from '@angular/forms';
import { ColumnCreateModel, ColumnAnswerCreateModel } from './columnCreateModel';

@Component({
  selector: 'app-column-dadcreate',
  templateUrl: './column-dadcreate.component.html',
  styleUrls: ['./column-dadcreate.component.scss']
})
export class ColumnDADCreateComponent extends CreateComponentBase implements OnInit {
  get answers(): any {
    return this.formGroup.get('answers'); 
  }

  columnModel: ColumnCreateModel[] = [];
  isAddNewColumnMode: boolean;
  newColumnNameDirty: boolean;
  tempAnswer: any;

  constructor(fb: UntypedFormBuilder) {
    super(fb);

  }

  ngOnInit(): void {
    this.formGroup = this.buildForm();
  }
  buildForm(): UntypedFormGroup {
    return this.fb.group({
      question: [null, Validators.required],
      answers: this.fb.array([this.createColumn(), this.createColumn()])
    });
  }

  updateEditDate() {
    this.formGroup = this.buildForm();
    if (this.editData == null) {
      return;
    }
    if (this.editData.questionText != null) {
      this.formGroup.controls.question.setValue(this.editData.questionText);
    }
    if (this.editData.answer != null) {
      let answers :ColumnCreateModel[] = JSON.parse(this.editData.answer);

      this.formGroup.controls.answers = this.fb.array([]);
      for (let i = 0; i < answers.length; i++) {
        const element = answers[i];
        (this.formGroup.controls.answers as UntypedFormArray).push(this.createColumn(element.columnName,element.columnAnswers));
      }
    }
  }
  addColumnClick() {
    // this.isAddNewColumnMode = true;
    // this.formGroup.controls.newColumnName.setValue("");
    // this.tempAnswer = [];
    // const newValue = this.formGroup.controls.answers.value.push(this.createColumn());

    //  this.formGroup.controls.answers.value.setValue(newValue);
    (this.formGroup.controls.answers as UntypedFormArray).push(this.createColumn())
    // this.columnModel.push({
    //   columnName: 'test',
    //   answers: ['test1', 'test2']
    // });
  }
  removeAnswer(index: number) {
    (this.formGroup.controls.answers as UntypedFormArray).removeAt(index);
  }
  createColumn(columnName: string = '', answers: ColumnAnswerCreateModel[] = []) {
    if (answers.length === 0) {
      return this.fb.group({
        columnName: [columnName, Validators.required],
        columnAnswers: this.fb.array([this.createColumnAnswers()])
      });
    } else {
      const answerFG=this.fb.group({
        columnName: [columnName, Validators.required],
        columnAnswers: null
      });
      answerFG.controls.columnAnswers = this.fb.array([]);
      for (let i = 0; i < answers.length; i++) {
        const value = answers[i];
        (answerFG.controls.columnAnswers as UntypedFormArray).push(this.createColumnAnswers(value.answer));
      }
      return answerFG;
    }
  }
  createColumnAnswers(answer: string = '') {
    return this.fb.group({
      answer: [answer, Validators.required]
    });
  }

  addAnswerClick() {
    this.tempAnswer.push(this.formGroup.controls.newAnswer.value);
    this.formGroup.controls.newAnswer.setValue("");
  }
  submitClicked(): UntypedFormGroup {
    this.formGroup.markAsTouched();
    if (this.formGroup.invalid) {
      return undefined;
    }
    return this.formGroup;
  }
}
