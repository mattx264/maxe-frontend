export interface ColumnCreateModel {
    columnName: string;
    columnAnswers: ColumnAnswerCreateModel[];
}
export interface ColumnAnswerCreateModel {
    answer: string;
}