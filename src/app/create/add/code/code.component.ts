import { Component, OnInit } from '@angular/core';

import { CreateComponentBase } from '../createComponentBase';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CodeEditorModel } from 'src/app/shared/code-editor/types';

@Component({
  selector: 'app-code',
  templateUrl: './code.component.html',
  styleUrls: ['./code.component.scss']
})
export class CodeComponent extends CreateComponentBase implements OnInit {

  codeModel: CodeEditorModel = {
    uri: 'inmemory://model',
    language: '',
    value: ''
  }
  codeModelTemplate: CodeEditorModel = {
    uri: 'inmemory://template',
    language: '',
    value: ''
  }
  showMonacoEditor = false;
  constructor(protected fb: FormBuilder) {
    super(fb);
  }

  ngOnInit() {
    if (this.formGroup !== undefined && this.formGroup !== null) {
      return;
    }
    this.formGroup = this.buildForm();
    this.formGroup.get('codeLanguage').valueChanges.subscribe(() => {

      const tempCodeLanguage = this.formGroup.get('codeLanguage').value;
      if (tempCodeLanguage === null) {
        return;
      }
      //this.showMonacoEditor = false;
      this.codeModel.language = tempCodeLanguage;
      this.codeModelTemplate.language = tempCodeLanguage;
      setTimeout(() => {
       // this.showMonacoEditor = true;
        // time for rerender monaco componenet after lang is change.       
        
      }, 100);
    });
  }
  buildForm(): FormGroup {
    return this.fb.group({
      question: ['', Validators.required],
      codeLanguage: [null, Validators.required],
      isTemplate: [false]
    });

  }
  updateEditDate(){
    
  }
  submitClicked(): FormGroup {
    this.formGroup.markAsTouched();
    if (this.formGroup.invalid) {
      return undefined;
    }
    return this.formGroup;
  }
}
