
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AuthService } from '../../auth/services/auth.service';
import { Test, QuestionBase, TestCategoryViewModel } from 'src/app/models/test';
import { HttpClientService } from '../../shared/services/http-client.service';
import { TestViewModel } from '../../viewModels/test-view-model';
import { Router } from '@angular/router';
import { NestedTreeControl } from '@angular/cdk/tree';
import { MatTreeNestedDataSource } from '@angular/material/tree';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {
  formGroup: FormGroup;

  initInfoShow = true;// this value is true -> for testing you can set it for false
  reviewShow = false;
  submitData: Test;
    nestedDataSource: any;
  nestedTreeControl: NestedTreeControl<TestCategoryViewModel>;
  testCategorysIds: TestCategoryViewModel[];

  constructor(private router: Router,private fb: FormBuilder, 
    private authService: AuthService, private httpClient: HttpClientService) { }
  ngOnInit() {
    this.formGroup = this.buildForm();
    this.nestedTreeControl = new NestedTreeControl<TestCategoryViewModel>(node => node.subTestCategories);
    this.nestedDataSource = new MatTreeNestedDataSource<TestCategoryViewModel>();

    this.httpClient.get('testCategory').subscribe(next => {
      this.testCategorysIds = JSON.parse(JSON.stringify(next));
      this.nestedDataSource.data = this.testCategorysIds;
    });  }
  buildForm(): FormGroup {
    return this.fb.group({
      name: [null, Validators.required],
      description: [null, Validators.required],
      isPublic: [true],
      testCategoryId:[null,Validators.required]
    });
  }
  setupClicked() {
    if (this.initInfoShow === true) {
      this.initInfoShow = false;
      this.submitData = this.formGroup.getRawValue();
    } else {

    }

  }
  questionsDone(data: QuestionBase[]) {

    this.submitData.questions = data;
    this.reviewShow = true;
  }
  addToList(id) {
    this.formGroup.get('testCategoryId').setValue(id);
    /*TODO - make multi select of test category ids
    if (this.testCategorysIds.includes(id)) {
      this.testCategorysIds.splice(this.testCategorysIds.indexOf(id), 1);
    } else {
      this.testCategorysIds.push(id);
    }*/
  }
  saveTestClick() {
    //
    let testVM: TestViewModel = {
      name: this.submitData.name,
      description: this.submitData.description,
      isPublic: this.submitData.isPublic,
      testCategoryId:this.submitData.testCategoryId,
      questions: []
    }
    for (let i = 0; i < this.submitData.questions.length; i++) {
      const element = this.submitData.questions[i];
      testVM.questions.push({
        questionTypeId: element.questionTypeId,
        questionText: element.questionText,
        answer: JSON.stringify(element.answer)// JSON.st
      })
    }

    this.httpClient.post('test', testVM).subscribe(result => {
      this.router.navigate(['/create/list']);
    });

  }
}
