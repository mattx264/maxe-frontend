import { QuestionUpsertViewModel } from "./question-upsert-view-model";

export interface CreateTestViewModel {
    id: number;
    name: string;
    description: string;
    isPrivate: boolean;
    testCategoryId: number;
    questions: QuestionUpsertViewModel[]
}