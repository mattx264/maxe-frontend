export interface QuestionUpsertViewModel {
    id?: number;
    testtId: number;
    questionText: string;
    answer: any;
    questionTypeId: number;
}