import { FormGroup } from '@angular/forms';
export interface QuestionCreateModel {
    questionType: number;
    formGroup: FormGroup;
    editData?: { id: number, answer: string, questionText: string, questionType: number };
}

