import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SaveBeforeDialogComponent } from './save-before-dialog.component';

describe('SaveBeforeDialogComponent', () => {
  let component: SaveBeforeDialogComponent;
  let fixture: ComponentFixture<SaveBeforeDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SaveBeforeDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaveBeforeDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
