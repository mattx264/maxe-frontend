import { HttpClient, HttpEventType } from '@angular/common/http';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-uploader',
  templateUrl: './uploader.component.html',
  styleUrls: ['./uploader.component.scss'],
})
export class UploaderComponent {
  @Input()
  requiredFileType: string = '';
  @Input()
  url: string;
  @Output()
  uploadEvent = new EventEmitter<any>();

  fileName = '';
  uploadProgress: number;
  uploadSub: Subscription;
  message: any;

  constructor(private http: HttpClient) {}

  onFileSelected(event) {
    const file: File = event.target.files[0];

    if (file) {
      this.fileName = file.name;
      const formData = new FormData();
      formData.append('file', file);

      const upload$ = this.http
        .post(this.url, formData, {
          reportProgress: true,
          observe: 'events',
        })
        .pipe(finalize(() => this.reset()));

      this.uploadSub = upload$.subscribe(
        (event) => {
          if (event.type == HttpEventType.UploadProgress) {
            this.uploadProgress = Math.round(
              100 * (event.loaded / event.total)
            );
            console.log(event);
          } else if(event.type == HttpEventType.Response) {
            this.uploadEvent.emit(event);
            var httpEvent = event as any;
            if (httpEvent.status == 400) {
              this.message = httpEvent.error;
            }
            console.log(event);
          }
        },
        (error) => {
          this.uploadEvent.emit(error);
          if (error.status == 400) {
            this.message = error.error;
          }
        }
      );
    }
  }

  cancelUpload() {
    this.uploadSub.unsubscribe();
    this.reset();
  }

  reset() {
    this.uploadProgress = null;
    this.uploadSub = null;
  }
}
