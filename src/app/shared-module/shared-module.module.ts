import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UploaderComponent } from './componets/uploader/uploader.component';
import { MatIconModule } from '@angular/material/icon';
import { MatLegacyProgressBarModule as MatProgressBarModule } from '@angular/material/legacy-progress-bar';
import { MatLegacyButtonModule as MatButtonModule } from '@angular/material/legacy-button';
import { EnumToArrayPipe } from './pipes/enum-to-array.pipe';
import { ConfirmationModalComponent } from './componets/confirmation-modal/confirmation-modal.component';
import { MatLegacyDialogModule as MatDialogModule } from '@angular/material/legacy-dialog';

@NgModule({
  imports: [
    CommonModule,
    MatIconModule,
    MatProgressBarModule,
    MatButtonModule,
    MatDialogModule,
  ],
  declarations: [
    UploaderComponent,
    EnumToArrayPipe,
    ConfirmationModalComponent,
  ],
  exports: [UploaderComponent, EnumToArrayPipe],
  providers: [],
})
export class SharedModule {}
