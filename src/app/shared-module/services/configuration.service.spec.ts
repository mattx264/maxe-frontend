import { TestBed } from '@angular/core/testing';
import { ConfigurationModel } from 'src/app/presenter-module/models/confifurationModel';

import { ConfigurationService } from './configuration.service';

describe('ConfigurationService', () => {
  let service: ConfigurationService;

  beforeEach(() => {
    window.localStorage.clear();

    TestBed.configureTestingModule({});
    service = TestBed.inject(ConfigurationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  it('should getConfiguration', () => {
    expect(service.getConfiguration()).toBeNull();
  });
  it('should addConfiguration', () => {
    const config: ConfigurationModel = {
      configurationButtonModel: [
        {
          action: null,
          name: 'Play',
          x: 0,
          y: 0,
        },
      ],
      configurationGestureModel: [],
      configurationVoiceModel: [],
    };
    service.setConfiguration(config);
    let savedConfig = service.getConfiguration().configurationButtonModel[0];
    expect(savedConfig.name).toBe(config.configurationButtonModel[0].name);
  });
  it('should addConfiguration replace', () => {
    let config = {
      configurationButtonModel: [
        {
          action: null,
          name: 'Play',
          x: 0,
          y: 0,
        },
      ],
      configurationGestureModel: [],
      configurationVoiceModel: [],
    };
    service.setConfiguration(config);
    config.configurationButtonModel[0].x=1;

    service.setConfiguration(config);

    let savedConfig = service.getConfiguration().configurationButtonModel[0];
    expect(savedConfig.x).toBe(1);
  });
  it('should setConfiguration', () => {
    const config: ConfigurationModel = {
      configurationVoiceModel: [],
      configurationGestureModel: [],
      configurationButtonModel: [
        {
          action: null,
          name: 'Play',
          x: 0,
          y: 0,
        },
      ],
    };
    service.setConfiguration(config);
    let savedConfig = service.getConfiguration().configurationButtonModel;
    expect(savedConfig[0].name).toBe(config.configurationButtonModel[0].name);
  });
  it('should clear', () => {
    service.clear();
    let result = window.localStorage.getItem(service.config_key);
    expect(result).toBeNull();
  });
});
