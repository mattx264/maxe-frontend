import { TestBed } from '@angular/core/testing';

import { JwtInterceptorService } from './jwt-interceptor.service';
import { AppModule } from 'src/app/app.module';
import { AuthService } from 'src/app/auth/services/auth.service';

describe('JwtInterceptorService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports:[AppModule],
    providers:[AuthService,JwtInterceptorService]
  }));

  it('should be created', () => {
    const service: JwtInterceptorService = TestBed.get(JwtInterceptorService);
    expect(service).toBeTruthy();
  });
});
