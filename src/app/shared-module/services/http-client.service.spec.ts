import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClientService } from './http-client.service';
import { HttpParams } from '@angular/common/http';

describe('HttpClientService', () => {
  let service: HttpClientService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    httpTestingController = TestBed.inject(HttpTestingController);

    service = TestBed.inject(HttpClientService);
  });
  afterEach(() => {
    httpTestingController.verify();
  });
  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  it('should get data', () => {
    const question = {
      id: 1,
      question: 'q1',
      answer: 'a'
    };
    const urlAddress = 'test/get';
    service.get(urlAddress).subscribe(res => {
    });
    const req = httpTestingController.expectOne(service.apiUrl(urlAddress))
    expect(req.request.method).toEqual('GET');

  });
  it('should get data with param', () => {

    const question = {
      id: 1,
      question: 'q1',
      answer: 'a'
    };
    const urlAddress = 'test/get';
    var params = { test: 'test1' };
    service.getParams(urlAddress, params).subscribe(res => {
    });
    const req = httpTestingController.expectOne(service.apiUrl(urlAddress + '?test=test1'))
    expect(req.request.method).toEqual('GET');
    let reqParams = new HttpParams();
  });
  it('Should apiUrl', () => {
    let url = service.apiUrl("abc");
    expect(url).toBe(service.address+"abc");
  })

});
