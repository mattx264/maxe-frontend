import { Injectable, ErrorHandler } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class GlobalErrorHandlerService implements ErrorHandler {
  constructor() {}
  handleError(error) {
    console.log(error);
    if (error == null) {
      return;
    }
    if (error.status != 500) {
      throw error;
    }
    var node = document.createElement('div');
    node.innerHTML = `<div class="global-error" style='font-size:30px;color:red'>${error}<div>`;
    document.body.appendChild(node);
    // IMPORTANT: Rethrow the error otherwise it gets swallowed
    throw error;
  }
}
