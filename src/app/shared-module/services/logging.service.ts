import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoggingService {
  KEY = "logging";
  constructor() { }
  log(log: string) {
    let logs = this.getAll();
    let date = new Date();
    logs.push(date.toDateString() + log);
    localStorage.setItem(this.KEY, JSON.stringify(logs));
  }
  getAll(): string[] {
    let logs = window.localStorage.getItem(this.KEY);
    if (logs == null || logs.length == 0) {
      return [];
    }
    return JSON.parse(logs);
  }
}
