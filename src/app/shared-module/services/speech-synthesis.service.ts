import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class SpeechSynthesisService {
  speekSynthesis(utterance: SpeechSynthesisUtterance) {
    if (window.speechSynthesis) {
      speechSynthesis.speak(utterance);
    }
  }

  constructor() {}
  async speek(text: string, lang: string) {
    if (window.speechSynthesis) {
      const speech = new SpeechSynthesisUtterance(text);
      speech.lang = lang;
      speechSynthesis.speak(speech);
    }
  }
  getVoices(): Promise<SpeechSynthesisVoice[]> {
    return new Promise(function (resolve, reject) {
      let synth = window.speechSynthesis;
      let id;

      id = setInterval(() => {
        if (synth.getVoices().length !== 0) {
          resolve(synth.getVoices());
          clearInterval(id);
        }
      }, 10);
    });
  }
}
