import { TestBed } from '@angular/core/testing';

import { SpeechSynthesisService } from './speech-synthesis.service';

describe('SpeechSynthesisService', () => {
  let service: SpeechSynthesisService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SpeechSynthesisService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  it('should speak', () => {
    expect(service.speek("")).toBeUndefined();
  });
  it('should return voice list', async () => {
    let voices = await service.getVoices()
    let count = voices.length;
    // DONT KNOW WHY IS 0 
    expect(count).toBeGreaterThanOrEqual(0);
  });
});
