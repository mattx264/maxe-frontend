import { Injectable } from '@angular/core';
import { SkipQuestionModel } from '../../models/skipQuestionModel';
import { QuestionService } from 'src/app/presenter-module/services/question.service';
import { AnswerViewModel, ExamResultViewModel, ExamRunClient, ExamRunViewModel, QuestionClient, QuestionSessionViewModel, SaveExamRunViewModel } from 'src/app/api-client';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ProgressQuestionService {


  KEY = "LEARN_RUN"

  constructor(
    private questionService: QuestionService,
    private router: Router,
    private examRunClient: ExamRunClient,
    private questionClient: QuestionClient
  ) { }

  async getOptionExamModel(guid: string) {
    return await this.examRunClient.get(guid).toPromise();

  }
  goToResult(learnSessionGuid: string) {
    this.router.navigate(["home","result", learnSessionGuid]);

  }
  async setAnswer(guid: string, answer: string) {
    return await this.questionClient.answer(guid, answer).toPromise();
  }
  async getResult(guid: string): Promise<ExamResultViewModel[]> {
    return await this.questionClient.getResult(guid).toPromise();

  }
  async getCurrentQuestionAnswer(guid: string): Promise<AnswerViewModel[]> {
    return await this.questionClient.getCurrentQuestionAnswer(guid).toPromise();
  }

  // async getLearnSession(guid: string): Promise<ExamRunViewModel> {
  //   return await this.examRunClient.get(guid).toPromise();

  //   /*let examInfo = window.localStorage.getItem(this.KEY);
  //   if (examInfo == null) {
  //     return null;
  //   }
  //   let learnSessions = JSON.parse(examInfo) as LearnRunModel[];
  //   return learnSessions.find(x => x.guid == guid);*/
  // }
  async saveLearnSession(saveExamRunModel: SaveExamRunViewModel) {
    return await this.examRunClient.start(saveExamRunModel).toPromise();
  }
  /*saveLearnSession(saveExamRunModel: SaveExamRunModel): string {
    const guid = this.uuidv4();
    if (saveExamRunModel.examIds.length > 1) {
      throw new Error("Not Implemeted");
    }
    let questionList = this.questionService.getExam(saveExamRunModel.examIds[0]).questionList;
    if (saveExamRunModel.randomizeQuestions) {
      questionList = this.shuffle(questionList);
    }
    let examInfo: LearnRunModel = {
      guid: guid,
      questionRepeat: +saveExamRunModel.questionRepeat,
      learnRunQuestionModel: questionList.map<LearnRunQuestionModel>(x => {
        return {
          questionId: x.id,
          rememberQuestionModel: null,
          answerIndex: -1,
          isQuestionPlayed: false
        }
      }),
      currentQuestionIndex: 0
    };
    let learnRunModelJSON = window.localStorage.getItem(this.KEY);
    let learnSessions = JSON.parse(learnRunModelJSON) as LearnRunModel[];
    if (learnSessions == null) {
      learnSessions = [];
    }
    learnSessions.push(examInfo);
    localStorage.setItem(this.KEY, JSON.stringify(learnSessions));
    return guid;
  }*/
  // updateLearnSession(learnSessionParameter: ExamRunViewModel) {
  //   let learnSessions = JSON.parse(window.localStorage.getItem(this.KEY)) as LearnRunModel[];
  //   if (learnSessions == null) {
  //     learnSessions = [];
  //   }
  //   let learnSessionIndex = learnSessions.findIndex(x => x.guid == learnSessionParameter.guid);
  //   if (learnSessionIndex == -1) {
  //     throw new Error("Learn session not found");
  //   }
  //   learnSessions[learnSessionIndex] = learnSessionParameter;
  //   localStorage.setItem(this.KEY, JSON.stringify(learnSessions));

  // }

  // async updateLearnSessionQuestion(learnSessionQuestion: LearnRunQuestionModel, learnRunGuid: string) {
  //   throw new Error("NOT IMPLEMENTED")

  //   // var learnRun = await this.getLearnSession(learnRunGuid);
  //   // var learnRunQuestionIndex = learnRun.learnRunQuestionModel.findIndex(x => x.questionId == learnSessionQuestion.questionId);
  //   // learnRun.learnRunQuestionModel[learnRunQuestionIndex] = learnSessionQuestion;
  //   // this.updateLearnSession(learnRun);
  // }

  async getCurrentQuestion(guid: string): Promise<QuestionSessionViewModel> {
    return await this.questionClient.getQuestion(guid).toPromise();
    // let learnRun = this.getLearnSession(learnRunGuid);
    // let learnRunQuestion = learnRun.learnRunQuestionModel[learnRun.currentQuestionIndex];
    // let question = this.questionService.getQuestionById(learnRunQuestion.questionId);
    // return this.map(question, learnRunQuestion);
  }

  // getLearnRunViewModelByIndex(index: number, learnRunGuid: string): LearnnSessionViewModel {
  //   throw new Error("NOT IMPLEMENTED")

  //   // const learnRunQuestion = this.getLearnSession(learnRunGuid).learnRunQuestionModel[index];
  //   // const question = this.questionService.getQuestionById(learnRunQuestion.questionId);

  //   // return this.map(question, learnRunQuestion);
  // }
  async getNextQuestion(guid: string): Promise<QuestionSessionViewModel> {
    return await this.questionClient.getNextQuestion(guid).toPromise();

    // let learnRun = this.getLearnSession(learnRunGuid);
    // let currentQuestion = this.getCurrentQuestion(learnRunGuid);
    // if (learnRun.questionRepeat > 0) {
    //   return this.getNextRepeatQuestion(learnRun.questionRepeat, learnRun, currentQuestion, learnRunGuid);
    // }
    // if (learnRun.currentQuestionIndex + 1 === learnRun.learnRunQuestionModel.length) {
    //   return null;
    // } else {
    //   this.setCurrectQuestionIndex(++learnRun.currentQuestionIndex, learnRunGuid);
    //   return this.getCurrentQuestion(learnRunGuid);
    // }
    // currentQuestion.isQuestionPlayed = false;
    // currentQuestion.answerIndex = null;

    // this.setCurrectQuestionIndex(learnRun.currentQuestionIndex, learnRunGuid);
  }

  // private getNextRepeatQuestion(repeatQuestionEvery: number, learnRunModel: LearnRunModel, currentQuestion: LearnnSessionViewModel, learnRunGuid: string): LearnnSessionViewModel {
  //   //check if qestion are answers
  //   if (this.checkIfAllQestionsRemembed(learnRunModel))
  //     return null;
  //   //calculate diffrence between current index and max steps
  //   // 1. mark start point from last rememberd question
  //   let lastRememerdQestionIndex = -1;
  //   let maxRememerdQestionIndex = -1;
  //   for (let i = 0; i < learnRunModel.learnRunQuestionModel.length; i++) {
  //     const question = learnRunModel.learnRunQuestionModel[i];
  //     lastRememerdQestionIndex = i;
  //     if (question.rememberQuestionModel == null) {
  //       break;
  //     }
  //   }

  //   //2 find max where you can go
  //   // for 2 repeat
  //   // 1 2 3 4 5
  //   // m 0 m 0 0
  //   //   l

  //   // it is -1 because last remember question takes one.
  //   let maxCounter = learnRunModel.questionRepeat - 1;
  //   for (let i = lastRememerdQestionIndex + 1; i < learnRunModel.learnRunQuestionModel.length; i++) {
  //     const question = learnRunModel.learnRunQuestionModel[i];
  //     if (question.rememberQuestionModel == null) {
  //       maxCounter--;
  //     }
  //     if (maxCounter == 0) {
  //       maxRememerdQestionIndex = i;
  //       break;
  //     }
  //   }

  //   if (learnRunModel.currentQuestionIndex >= maxRememerdQestionIndex) {
  //     learnRunModel.currentQuestionIndex = lastRememerdQestionIndex;
  //   } else {
  //     for (let i = learnRunModel.currentQuestionIndex + 1; i < learnRunModel.learnRunQuestionModel.length; i++) {
  //       const question = learnRunModel.learnRunQuestionModel[i];
  //       if (question.rememberQuestionModel == null) {
  //         learnRunModel.currentQuestionIndex = i;
  //         break;
  //       }
  //     }
  //   }

  //   this.setCurrectQuestionIndex(learnRunModel.currentQuestionIndex, learnRunGuid);
  //   const learnRunViewModel = this.getCurrentQuestion(learnRunGuid);
  //   throw new Error("NOT IMPLEMENTED !!")
  //   // return learnRunViewModel;

  // }
  checkIfAllQestionsRemembed(learnRunModel): boolean {
    for (let i = 0; i < learnRunModel.learnRunQuestionModel.length; i++) {
      const question = learnRunModel.learnRunQuestionModel[i];
      if (question.rememberQuestionModel == null) {
        return false;
      }
    }
    return true;
  }

  async setCurrectQuestionIndex(index: number, learnRunGuid: string) {
    throw new Error("NOT IMPLEMENTED")

    // let learnSession = await this.getLearnSession(learnRunGuid);
    // learnSession.currentQuestionIndex = index;
    // this.updateLearnSession(learnSession);
  }

  // getCurrentQuestionIndex(): number {
  //   let lastQuestionIndex = window.localStorage.getItem('lastQuestionIndex');
  //   if (lastQuestionIndex == null) {
  //     return null;
  //   }
  //   return +lastQuestionIndex;
  // }

  markQuestionRemember(learnRunViewModel: QuestionSessionViewModel, learnRunGuid: string): QuestionSessionViewModel {
    /*var learnRun = this.getLearnSession(learnRunGuid);
    var learnRunQuestion = learnRun.learnRunQuestionModel.find(x => x.questionId == learnRunViewModel.questionId);
    if (learnRunQuestion?.rememberQuestionModel != null) {
      return;
    }
    learnRunQuestion.rememberQuestionModel = {
      setDate: new Date()
    }
    learnRunViewModel.isMarkAsRemembered = true;
    this.updateLearnSessionQuestion(learnRunQuestion, learnRunGuid);*/
    return learnRunViewModel;
  }



  setSkip(skipQuestionModel: SkipQuestionModel) {
    throw new Error("NOT IMPLEMENTED")
    // let skips = this.getSkip();
    // let skipQuestionIndex = skips.findIndex(x => x.questionId == skipQuestionModel.questionId);
    // if (skipQuestionIndex > -1) {
    //   skips = skips.splice(skipQuestionIndex, 1);
    // }
    // skips.push(skipQuestionModel);
    // window.localStorage.setItem('skipQuestions', JSON.stringify(skips));

  }

  // getRemembers(learnRunGuid: string): RememberQuestionModel[] {
  //   var learnRun = this.getExamRun(learnRunGuid);
  //   let remembers = learnRun.learnRunQuestionModel.filter(x => x.rememberQuestionModel != null);
  //   if (remembers == null)
  //     return [];
  //   return remembers.map(x => x.rememberQuestionModel);
  // }

  getSkip(): SkipQuestionModel[] {
    let skips = window.localStorage.getItem('skipQuestions');
    if (skips == null)
      return [];
    return JSON.parse(skips) as SkipQuestionModel[];
  }
  private uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }
  private shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }

    return array;
  }
  // private map(question: QuestionModel, learnRunQuestion: LearnRunQuestionModel): LearnnSessionViewModel {
  //   return {
  //     answers: question.answers,
  //     questionSections: question.questionSections,
  //     questionId: question.id,
  //     answerIndex: learnRunQuestion.answerIndex,
  //     isMarkAsRemembered: learnRunQuestion.rememberQuestionModel != null ? true : false,
  //     isQuestionPlayed: learnRunQuestion.isQuestionPlayed,
  //     questionType: question.type,
  //     questionSubType: question.subType
  //   };
  // }
}
