import { Injectable, EventEmitter } from '@angular/core';
declare var webkitSpeechRecognition: any;
declare var webkitSpeechGrammarList: any;
declare var webkitSpeechRecognitionEvent: any;
@Injectable({
  providedIn: 'root'
})

export class SpeechRecognitionService {
  //in future add api to call speech to text if browser not support
  isSpeechSuppoerByBrowser: boolean;

  //recognition: SpeechRecognition;
  //speechRecognitionList: SpeechGrammarList;
  onResult = new EventEmitter<string>();
  // constructor() {

  //   var colors = ['aqua', 'azure', 'beige', 'bisque', 'black', 'blue', 'brown', 'chocolate', 'coral'];
  //   var grammar = '#JSGF V1.0; grammar colors; public <color> = ' + colors.join(' | ') + ' ;'
  //   try {
  //     var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition;
  //     var SpeechGrammarList = SpeechGrammarList || webkitSpeechGrammarList;
  //     var SpeechRecognitionEvent = SpeechRecognitionEvent || webkitSpeechRecognitionEvent;
  //   } catch (error) {
  //     this.isSpeechSuppoerByBrowser = false;
  //     return;
  //   }
  //   this.isSpeechSuppoerByBrowser = true;

  //   if (SpeechRecognition == undefined) {
  //     return;
  //   }
  //   this.recognition = new SpeechRecognition();
  //   this.speechRecognitionList = new SpeechGrammarList();

  //   this.speechRecognitionList.addFromString(grammar, 1);


  //   this.recognition.grammars = this.speechRecognitionList;
  //   this.recognition.continuous = false;
  //   this.recognition.lang = 'en-US';
  //   this.recognition.interimResults = false;
  //   this.recognition.maxAlternatives = 1;

  //   this.recognition.onresult = (event) => {
  //     console.log(event.results);
  //     let results = event.results;
  //     for (let i = 0; i < results.length; i++) {
  //       const element = results[i];
  //       for (let j = 0; j < element.length; j++) {
  //         const t = element[j];
  //         console.log(t.transcript);
  //         this.onResult.emit(t.transcript);
  //       }
  //     }
  //   }
  //   this.recognition.onspeechend = () => {
  //     console.log('onspeechend')
  //     this.recognition.stop();
  //   }

  //   this.recognition.onnomatch = (event) => {
  //     // diagnostic.textContent = 'I didnt recognise that color.';
  //     console.log(event);
  //   }
  //   this.recognition.onerror = (event) => {
  //     console.log(event);
  //   }
  // }
  // startListen() {
  //   this.recognition.start();

  // }
  // stopListen() {
  //   this.recognition.stop();
  // }
  // startListenContinures() {
  //   this.recognition.continuous = true;
  //   this.recognition.start();
  // }
  // onresult(this: SpeechRecognition, ev: SpeechRecognitionEvent) {
  //   console.log(this);
  //   console.log(ev);
  // }

}
/*
SpeechRecognition.continuous: Controls whether continuous results are captured (true), or just a single result each time recognition is started (false).
SpeechRecognition.lang: Sets the language of the recognition. Setting this is good practice, and therefore recommended.
SpeechRecognition.interimResults: Defines whether the speech recognition system should return interim results, or just final results. Final results are good enough for this simple demo.
SpeechRecognition.maxAlternatives: Sets the number of alternative potential matches that should be returned per result. This can sometimes be useful, say if a result is not completely clear and you want to display a list if alternatives for the user to choose the correct one from. But it is not needed for this simple demo, so we are just specifying one (which is actually the default anyway.)
*/
