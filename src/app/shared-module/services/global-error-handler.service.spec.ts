import { TestBed } from '@angular/core/testing';

import { GlobalErrorHandlerService } from './global-error-handler.service';

describe('GlobalErrorHandlerService', () => {
  let service: GlobalErrorHandlerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GlobalErrorHandlerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should throw error', () => {
    expect(() => {
      service.handleError({status:500, error:'BlaBla'});
    }).toThrow({status:500, error:'BlaBla'});
    let nodes = document.querySelectorAll('.global-error');
    expect(nodes.length).toBe(1);
  });
});
