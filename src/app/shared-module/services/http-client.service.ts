import { Injectable } from '@angular/core';
import { HttpParams, HttpClient } from '@angular/common/http';
import { throwError as observableThrowError, Observable } from 'rxjs';
import { catchError, first, finalize, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';



@Injectable({
  providedIn: 'root'
})
export class HttpClientService {
  address: string;

  constructor(private http: HttpClient) { 
    if (environment.production === true) {
      this.address = '/api/';
    } else {
      this.address = 'https://localhost:44321/api/';
    }

  }
  get(url: string): Observable<Object> {
    return this.http.get(this.apiUrl(url)).pipe(map(data => {
      return data;
    }), catchError(this.handleError), finalize(() => {
    }), first());
  }
  getParams(url: string, params: Object): Observable<Object> {
    let reqParams = new HttpParams();
    for (const item in params) {
      if (item !== undefined) {
        if (params[item] !== null && params[item] !== 'null') {
          reqParams = reqParams.append(item, params[item]);
        }
      }

    }
    return this.http.get(this.apiUrl(url), { params: reqParams }).pipe(map(data => {
      return data;
    }, catchError(this.handleError)), first());

  }
  public handleError = (error: any) => {

    // Do messaging and error handling here
    if (error.status === 500) {
      // this.toasterService.pop('error', error.message);
      throw new Error(error.message);
    }
    // Unauthorize
    if (error.status === 401) {
     // this.authService.unauthorize();
    }
    return observableThrowError(error);
  }

  apiUrl(url: string): string {
    return this.address + url;
  }

}
