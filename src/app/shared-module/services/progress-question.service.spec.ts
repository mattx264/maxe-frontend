import { TestBed } from '@angular/core/testing';

import { ProgressQuestionService } from './progress-question.service';
import { QuestionService } from '../../presenter-module/services/question.service';
import { ServerData } from 'src/app/mock-server/serverModels';
import {
  ExamRunClient,
  ExamSearchClient,
  QuestionClient,
} from 'src/app/api-client';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';

describe('ProgressQuestionService', () => {
  let service: ProgressQuestionService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, RouterTestingModule],
      providers: [ExamSearchClient, ExamRunClient, QuestionClient],
    });
    localStorage.clear();
    const questionService = TestBed.inject(QuestionService);
    ServerData.exams = [
      {
        id: 1,
        title: 'learn',
        categoryId: 1,
        description: '',
        imagePath: '',
        tags: [''],
        isButtonFree: false,
        questionList: [
          {
            id: 1,
            questionSections: [{ value: 'q1', type: 'TEXT' }],
            correctAnswer: { type: 'TEXT', value: 'a1' },
            answerList: [
              {
                difficultyLevel: 'Normal',
                wrongAnswers: [{ type: 'TEXT', value: 'a2' }],
                id: 1,
                type: 'QUIZ',
              },
            ],
          },
          {
            id: 2,
            questionSections: [{ value: 'q2', type: 'TEXT' }],
            correctAnswer: { type: 'TEXT', value: 'a1' },
            answerList: [
              {
                difficultyLevel: 'Normal',
                wrongAnswers: [{ type: 'TEXT', value: 'a2' }],
                id: 1,
                type: 'QUIZ',
              },
            ],
          },
          {
            id: 3,
            questionSections: [{ value: 'q3', type: 'TEXT' }],
            correctAnswer: { type: 'TEXT', value: 'a1' },
            answerList: [
              {
                difficultyLevel: 'Normal',
                wrongAnswers: [
                  { type: 'TEXT', value: 'a2' },
                  { type: 'TEXT', value: 'a3' },
                ],
                id: 1,
                type: 'QUIZ',
              },
            ],
          },
          {
            id: 4,
            questionSections: [{ value: 'q4', type: 'TEXT' }],
            correctAnswer: { type: 'TEXT', value: 'a1' },
            answerList: [
              {
                difficultyLevel: 'Normal',
                wrongAnswers: [
                  { type: 'TEXT', value: 'a2' },
                  { type: 'TEXT', value: 'a3' },
                  { type: 'TEXT', value: 'a4' },
                ],
                id: 1,
                type: 'QUIZ',
              },
            ],
          },
          {
            id: 5,
            questionSections: [{ value: 'q5', type: 'TEXT' }],
            correctAnswer: { type: 'TEXT', value: 'a1' },
            answerList: [
              {
                difficultyLevel: 'Normal',
                wrongAnswers: [{ type: 'TEXT', value: 'a2' }],
                id: 1,
                type: 'QUIZ',
              },
            ],
          },
        ],
      },
      {
        id: 2,
        title: 'learn2',
        categoryId: 1,
        description: '',
        imagePath: '',
        tags: [''],
        isButtonFree: false,
        questionList: [
          {
            id: 6,
            questionSections: [{ value: 'q1', type: 'TEXT' }],
            correctAnswer: { type: 'TEXT', value: 'a1' },
            answerList: [
              {
                difficultyLevel: 'Normal',
                wrongAnswers: [{ type: 'TEXT', value: 'a2' }],
                id: 1,
                type: 'QUIZ',
              },
            ],
          },
        ],
      },
    ];

    service = TestBed.inject(ProgressQuestionService);
    localStorage.clear();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  /* THIS TEST HAVE TO MOVED TO BACKEND
  it('should save option and get it', () => {
    const examId = 2;
    const saveExamRunModel: SaveExamRunViewModel = new SaveExamRunViewModel({
      examIds: [examId],
      questionRepeat: 3,
      questionLimit: 0,
      randomizeQuestions: false
    });
    const guid = service.saveLearnSession(saveExamRunModel);
    let output = service.getLearnSession(guid);
    let result: LearnRunModel = {
      guid: guid,
      questionRepeat: 3,
      //4, 1, 3, 2
      learnRunQuestionModel: [{
        questionId: 6, rememberQuestionModel: null, answerIndex: -1, isQuestionPlayed: false
      }], currentQuestionIndex: 0
    };
    expect(result).toEqual(output);
  });
  it('should save option and get it - test 2', () => {
    const examId = 2;
    const saveExamRunModel: SaveExamRunModel = {
      examIds: [examId],
      questionRepeat: 3,
      questionLimit: 0,
      randomizeQuestions: true
    }
    const guid = service.saveLearnSession(saveExamRunModel);
    let output = service.getLearnSession(guid);
    let result: LearnRunModel = {
      guid: guid,
      questionRepeat: 3,
      learnRunQuestionModel: [{
        questionId: 6, rememberQuestionModel: null, answerIndex: -1, isQuestionPlayed: false
      }],
      currentQuestionIndex: 0
    };
    expect(result).toEqual(output);
  });
  it('should get next 2 repeat question', () => {
    const saveExamRunModel: SaveExamRunModel = {
      examIds: [1],
      questionRepeat: 2,
      questionLimit: 0,
      randomizeQuestions: false
    }
    const guid = service.saveLearnSession(saveExamRunModel);
    let currentQuestion = service.getCurrentQuestion(guid);
    expect(currentQuestion.questionSections[0].value).toBe("q1");
    service.markQuestionRemember(currentQuestion, guid);
    currentQuestion = service.getNextQuestion(guid);
    expect(currentQuestion.questionSections[0].value).toBe("q2");
    service.markQuestionRemember(currentQuestion, guid);
    currentQuestion = service.getNextQuestion(guid);
    expect(currentQuestion.questionSections[0].value).toBe("q3");
    currentQuestion = service.getNextQuestion(guid);
    expect(currentQuestion.questionSections[0].value).toBe("q4");
    currentQuestion = service.getNextQuestion(guid);
    expect(currentQuestion.questionSections[0].value).toBe("q3");

  });

  it('should get next 2 repeat question', () => {
    const saveExamRunModel: SaveExamRunModel = {
      examIds: [1],
      questionRepeat: 2,
      questionLimit: 0,
      randomizeQuestions: false
    }
    const guid = service.saveLearnSession(saveExamRunModel);
    let currentQuestion = service.getCurrentQuestion(guid);
    expect(currentQuestion.questionSections[0].value).toBe("q1");
    currentQuestion = service.getNextQuestion(guid);

    expect(currentQuestion.questionSections[0].value).toBe("q2");

    currentQuestion = service.getNextQuestion(guid);

    expect(currentQuestion.questionSections[0].value).toBe("q1");
    currentQuestion = service.getNextQuestion(guid);

    expect(currentQuestion.questionSections[0].value).toBe("q2");
    currentQuestion = service.getNextQuestion(guid);

    expect(currentQuestion.questionSections[0].value).toBe("q1");

    service.markQuestionRemember(currentQuestion, guid);
    //service is mark questin as rember but not changing order

    expect(currentQuestion.questionSections[0].value).toBe("q1");
    currentQuestion = service.getNextQuestion(guid);

    expect(currentQuestion.questionSections[0].value).toBe("q2");
    currentQuestion = service.getNextQuestion(guid);

    expect(currentQuestion.questionSections[0].value).toBe("q3");
    currentQuestion = service.getNextQuestion(guid);

    expect(currentQuestion.questionSections[0].value).toBe("q2");
    currentQuestion = service.getNextQuestion(guid);

    expect(currentQuestion.questionSections[0].value).toBe("q3");

    service.markQuestionRemember(currentQuestion, guid);

    expect(currentQuestion.questionSections[0].value).toBe("q3");
    currentQuestion = service.getNextQuestion(guid);

    expect(currentQuestion.questionSections[0].value).toBe("q4");
    currentQuestion = service.getNextQuestion(guid);

    expect(currentQuestion.questionSections[0].value).toBe("q2");
  });

  it('should get next 2 repeat question but remembeerd quesiton is last question in loop. Question 0 is never remeberd', () => {
    const saveExamRunModel: SaveExamRunModel = {
      examIds: [1],
      questionRepeat: 2,
      questionLimit: 0,
      randomizeQuestions: false
    }
    const guid = service.saveLearnSession(saveExamRunModel);
    let currentQuestion = service.getCurrentQuestion(guid);

    expect(currentQuestion.questionSections[0].value).toBe("q1");
    currentQuestion = service.getNextQuestion(guid);

    expect(currentQuestion.questionSections[0].value).toBe("q2");

    service.markQuestionRemember(currentQuestion, guid);
    currentQuestion = service.getNextQuestion(guid);

    expect(currentQuestion.questionSections[0].value).toBe("q3");
    currentQuestion = service.getNextQuestion(guid);

    expect(currentQuestion.questionSections[0].value).toBe("q1");
    currentQuestion = service.getNextQuestion(guid);

    expect(currentQuestion.questionSections[0].value).toBe("q3");

    service.markQuestionRemember(currentQuestion, guid);
    currentQuestion = service.getNextQuestion(guid);

    expect(currentQuestion.questionSections[0].value).toBe("q4");
    currentQuestion = service.getNextQuestion(guid);

    expect(currentQuestion.questionSections[0].value).toBe("q1");
    currentQuestion = service.getNextQuestion(guid);

    expect(currentQuestion.questionSections[0].value).toBe("q4");

    service.markQuestionRemember(currentQuestion, guid);

    currentQuestion = service.getNextQuestion(guid);

    expect(currentQuestion.questionSections[0].value).toBe("q5");
    currentQuestion = service.getNextQuestion(guid);

    expect(currentQuestion.questionSections[0].value).toBe("q1");
    currentQuestion = service.getNextQuestion(guid);

    expect(currentQuestion.questionSections[0].value).toBe("q5");
    service.markQuestionRemember(currentQuestion, guid);

    currentQuestion = service.getNextQuestion(guid);
    expect(currentQuestion.questionSections[0].value).toBe("q1");

    currentQuestion = service.getNextQuestion(guid);
    expect(currentQuestion.questionSections[0].value).toBe("q1");


    service.markQuestionRemember(currentQuestion, guid);
    currentQuestion = service.getNextQuestion(guid);
    //all questions remembed
    expect(currentQuestion).toBe(null);

  });

  it('should get next 3 repeat question', () => {
    const saveExamRunModel: SaveExamRunModel = {
      examIds: [1],
      questionRepeat: 3,
      questionLimit: 0,
      randomizeQuestions: false
    }
    const guid = service.saveLearnSession(saveExamRunModel);
    let currentQuestion = service.getCurrentQuestion(guid);

    expect(currentQuestion.questionSections[0].value).toBe("q1");
    currentQuestion = service.getNextQuestion(guid);

    expect(currentQuestion.answerIndex).toBe(-1);

    expect(currentQuestion.questionSections[0].value).toBe("q2");
    currentQuestion = service.getNextQuestion(guid);

    expect(currentQuestion.questionSections[0].value).toBe("q3");
    currentQuestion = service.getNextQuestion(guid);

    expect(currentQuestion.questionSections[0].value).toBe("q1");

    service.markQuestionRemember(currentQuestion, guid);
    currentQuestion = service.getNextQuestion(guid);

    // expect(component.lastIndexOfRememberdQuestion).toBe(1);
    expect(currentQuestion.questionSections[0].value).toBe("q2");
    currentQuestion = service.getNextQuestion(guid);

    expect(currentQuestion.questionSections[0].value).toBe("q3");
    currentQuestion = service.getNextQuestion(guid);

    expect(currentQuestion.questionSections[0].value).toBe("q4");
    currentQuestion = service.getNextQuestion(guid);

    expect(currentQuestion.questionSections[0].value).toBe("q2");
    currentQuestion = service.getNextQuestion(guid);

    expect(currentQuestion.questionSections[0].value).toBe("q3");
    currentQuestion = service.getNextQuestion(guid);

    expect(currentQuestion.questionSections[0].value).toBe("q4");
    currentQuestion = service.getNextQuestion(guid);

    expect(currentQuestion.questionSections[0].value).toBe("q2");

    service.markQuestionRemember(currentQuestion, guid);
    currentQuestion = service.getNextQuestion(guid);

    expect(currentQuestion.questionSections[0].value).toBe("q3");
    currentQuestion = service.getNextQuestion(guid);

    expect(currentQuestion.questionSections[0].value).toBe("q4");
    currentQuestion = service.getNextQuestion(guid);

    expect(currentQuestion.questionSections[0].value).toBe("q5");

  });*/
});
