import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LayoutService {

  isBackButtonToRoot: boolean;

  constructor(private router: Router) { }

  setBackButtonToRoot() {
    this.isBackButtonToRoot = true;
  }
  onBackButton() {
    if (this.isBackButtonToRoot === true) {
      this.isBackButtonToRoot = false;
      this.router.navigate([""]);
      window.location.replace(this.router.url)
      return;
    }
    window.history.back();
  }
}
