import { Injectable } from '@angular/core';
import { ButtonConfigurationModel } from '../../models/confifurationModel'
import { VoiceSettingsModel } from '../../models/voiceSettingsModel';
import { ConfigurationModel } from '../../presenter-module/models/confifurationModel';
@Injectable({
  providedIn: 'root'
})
export class ConfigurationService {

  //#region prop
  config_key = "config";
  config_voice_key: "voice_config";
  //#endregion
  constructor() { }
  getConfiguration(): ConfigurationModel {
    const value = window.localStorage.getItem(this.config_key);
    if (value == null) {
      return null;
    }
    try {
      const configurationModel = JSON.parse(value) as ConfigurationModel;
      return configurationModel;
    } catch (error) {
      this.clear();
      return null;
    }
  }
  clear() {
    window.localStorage.removeItem(this.config_key);
  }

  setConfiguration(values: ConfigurationModel) {
    window.localStorage.setItem(this.config_key, JSON.stringify(values));
  }
  saveVoiceSettigs(voice: VoiceSettingsModel) {
    window.localStorage.setItem(this.config_voice_key, JSON.stringify(voice));

  }
}
