import { TestBed } from '@angular/core/testing';

import { LoggingService } from './logging.service';

describe('LoggingService', () => {
  let service: LoggingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LoggingService);
    localStorage.clear();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  it('should log', () => {
    service.log("test log");
    let logs = service.getAll();
    expect(logs[0].indexOf("test log")).toBeGreaterThan(0)
  });
  it('should getAll', () => {
    let logs = service.getAll();
    expect(logs.length).toBe(0)

  });
});
