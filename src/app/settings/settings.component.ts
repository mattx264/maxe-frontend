import { Component, OnInit } from '@angular/core';
import { SpeechSynthesisService } from '../shared-module/services/speech-synthesis.service';
import { ConfigurationService } from '../shared-module/services/configuration.service';
import { VoiceSettingsModel } from '../models/voiceSettingsModel';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
  voices: SpeechSynthesisVoice[];
  voice: VoiceSettingsModel = { rate: 1, pitch: 1, voiceName: '' }
  constructor(private speechSynthesisService: SpeechSynthesisService,
     private configurationService: ConfigurationService) {
    this.getVoices();
  }

  ngOnInit(): void {

  }
  async getVoices() {
    this.voices = await this.speechSynthesisService.getVoices();
    this.voices = this.voices.filter(x => x.lang === "en-US");
  }
  voiceClick(voice: SpeechSynthesisVoice) {
    let utterance = new SpeechSynthesisUtterance("This is test");
    utterance.voice = voice;
    this.voice.voiceName = voice.name;
    utterance.rate = this.voice.rate;
    utterance.pitch = this.voice.pitch;
    utterance.text = "this not is test";
    this.speechSynthesisService.speekSynthesis(utterance);
  }

  saveVoice() {
    this.configurationService.saveVoiceSettigs(this.voice);
  }
}
