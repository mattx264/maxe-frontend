import { ComponentFixture, TestBed, inject, waitForAsync } from '@angular/core/testing';

import { SettingsComponent } from './settings.component';
import { SpeechSynthesisService } from '../shared-module/services/speech-synthesis.service';
import { MaterialModule } from '../shared-module/material-module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';

describe('SettingsComponent', () => {
    let component: SettingsComponent;
    let fixture: ComponentFixture<SettingsComponent>;
    //let speechSynthesisService: SpeechSynthesisService;
    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [MaterialModule, NoopAnimationsModule,RouterTestingModule],
            declarations: [SettingsComponent],
            providers: [SpeechSynthesisService ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        //speechSynthesisService = TestBed.get(SpeechSynthesisService);
        fixture = TestBed.createComponent(SettingsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
    it('should voice Click',  inject([SpeechSynthesisService], 
        async (speechSynthesisService: SpeechSynthesisService)  => {
        let voice: SpeechSynthesisVoice = {
            default: true,
            lang: "en-US",
            localService: true,
            name: "Microsoft David Desktop - English (United States)",
            voiceURI: "Microsoft David Desktop - English (United States)"
        };
        let voices = await speechSynthesisService.getVoices();
        spyOn(speechSynthesisService, 'speekSynthesis').and.callFake((data) => {
            expect(data).not.toBeNull();
        });
        component.voiceClick(voices[1]);
    }));
});
