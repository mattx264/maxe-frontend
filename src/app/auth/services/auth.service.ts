import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { UserViewModel } from 'src/app/api-client';
import { addUser } from 'src/app/store/user.actions';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private user: UserViewModel;
  constructor(private router: Router, private store: Store) {
    this.user = JSON.parse(
      localStorage.getItem('currentUser')
    ) as UserViewModel;
    if (this.user == null || this.user.firstName == null) {
      this.user = undefined;
    } else {
      this.store.dispatch(addUser({ user: this.user }));
    }
  }
  doGoogleLogin(): void {
    // return new Promise<any>((resolve, reject) => {
    //   firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL).then(() => {
    //     let provider = new firebase.auth.GoogleAuthProvider();
    //     provider.addScope('profile');
    //     provider.addScope('email');
    //     this.afAuth.auth
    //       .signInWithPopup(provider)
    //       .then(res => {
    //         resolve(res);
    //       })
    //   })
    // });
  }
  getCurrentUser(): UserViewModel {
    return this.user;
  }
  setsCurrentUser(user: UserViewModel): void {
    this.user = user;
    localStorage.setItem('currentUser', JSON.stringify(user));
    //this.store.dispatch(addUser({ user: user }));
  }
  logout(): void {
    localStorage.removeItem('currentUser');
    this.store.dispatch(addUser({ user: null }));
    this.user = null;
    this.router.navigate(['/']);
  }
  unauthorize(): void {
    localStorage.removeItem('currentUser');

    this.router.navigate(['/login'], {
      queryParams: { returnUrl: this.router.url },
    });
  }
  getToken(): string {
    if (this.user == null) {
      return '';
    }
    return this.user.token;
  }
}
