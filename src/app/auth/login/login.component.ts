import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginClient } from 'src/app/api-client';
import { select, Store } from '@ngrx/store';
import { login } from 'src/app/store/user.actions';
import { errorMessageSelector } from 'src/app/store/shared/shared.selectors';
import { AppState } from 'src/app/store/app.state';

const RETURN_URL = 'returnUrl';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  formGroup: UntypedFormGroup;
  returnUrl: any;
  unathorized = false;
  loginFailed$ = this.store.pipe(select(errorMessageSelector));
  constructor(
    private authService: AuthService,
    private fb: UntypedFormBuilder,
    private loginClient: LoginClient,
    private route: ActivatedRoute,
    private router: Router,
    private store: Store<AppState>
  ) {}

  ngOnInit(): void {
    this.buildForm();
    // get return url from route parameters or default to '/'

    this.returnUrl = this.route.snapshot.queryParams[RETURN_URL] || '/';
  }
  buildForm(): void {
    this.formGroup = this.fb.group({
      email: [
        null,
        Validators.compose([Validators.required, Validators.email]),
      ],
      password: [null, Validators.required],
    });
  }
  login(): void {
    this.unathorized = false;
    this.store.dispatch(
      login({
        credentials: this.formGroup.getRawValue(),
        returnUrl: this.returnUrl,
      })
    );
    // this.loginClient
    //   .createToken(this.formGroup.getRawValue())
    //   .toPromise()
    //   .then(
    //     (response: UserViewModel) => {
    //       this.authService.setsCurrentUser(response);
    //       this.router.navigate([this.returnUrl]);
    //     },
    //     (error: HttpErrorResponse) => {
    //       if (error.status === 401) {
    //         this.unathorized = true;
    //       }
    //     }
    //   );
  }
  googleLoginClick(): void {
    this.authService.doGoogleLogin();
  }

  forgotPassword(): void {
    return;
  }

  singUp() {
    this.router.navigate(['/registration']);
  }
}
