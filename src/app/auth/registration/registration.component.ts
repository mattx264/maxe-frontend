import { EmailExistsValidator } from './../validators/email-exists-validator';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import {
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserRegistrationClient } from 'src/app/api-client';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss'],
})
export class RegistrationComponent implements OnInit {
  registerForm: UntypedFormGroup;

  constructor(
    private authService: AuthService,
    private fb: UntypedFormBuilder,
    private userRegistrationClient: UserRegistrationClient,
    private router: Router
  ) {}

  ngOnInit() {
    this.buildForm();
    //  this.authService.logout();
  }

  buildForm() {
    this.registerForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: [
        '',
        [Validators.required, Validators.email],
        EmailExistsValidator.emailExists(this.userRegistrationClient),
      ],
      password: [
        '',
        Validators.compose([Validators.required, Validators.minLength(6)]),
      ],
    });
  }

  registerNewUser() {
    const userToCreate = this.registerForm.value;
    userToCreate.timeZone = new Date();
    this.userRegistrationClient
      .registration(userToCreate)
      .toPromise()
      .then((response: any) => {
        this.router.navigate(['']);
      });
  }
}
