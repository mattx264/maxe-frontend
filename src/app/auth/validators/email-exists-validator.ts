import { AbstractControl} from '@angular/forms';

import { UserRegistrationClient } from 'src/app/api-client';

export class EmailExistsValidator {
  static emailExists(userRegistrationClient: UserRegistrationClient) {
    return (control: AbstractControl) => {
      const emailRegex = new RegExp(/\S+@\S+\.\S+/);

      if (emailRegex.test(control.value)) {
        userRegistrationClient
          .checkEmail(control.value)
          .toPromise()
          .then((result: any) => {
            if (result.message === 'Email exists') {
              return { emailExists: true };
            }
            return null;
          });
      }
    };
  }
}
