import { Component } from '@angular/core';
import { LayoutService } from './shared-module/services/layout.service';
declare var screen: any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'maxe';
  isGoBack = history.length > 0 ? true : false;
  constructor(private layoutService: LayoutService) {
    // NOT WORKING AT ALL
    // if (screen && screen.orientation &&  screen.orientation.lock) {
    //   screen.orientation.lock("landscape");
    // }
  }
  goBack() {
    this.layoutService.onBackButton();
  }
}
