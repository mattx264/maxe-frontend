import {
  BrowserModule,
  HammerModule,
  HammerGestureConfig,
  HAMMER_GESTURE_CONFIG,
} from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SettingsComponent } from './settings/settings.component';
import { MaterialModule } from './shared-module/material-module';
import { GlobalErrorHandlerService } from './shared-module/services/global-error-handler.service';
import { SaveBeforeDialogComponent } from './shared-module/componets/save-before-dialog/save-before-dialog.component';
import * as Hammer from 'hammerjs';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import {
  API_BASE_URL,
  ExamRunClient,
  ExamSearchClient,
  LoginClient,
  QuestionClient,
  UserRegistrationClient,
} from './api-client';
import { LoginComponent } from './auth/login/login.component';
import { RegistrationComponent } from './auth/registration/registration.component';
import { ReactiveFormsModule } from '@angular/forms';
import { LayoutComponent } from './landing-page/layout/layout.component';
import { HeaderComponent } from './landing-page/layout/header/header.component';
import { FooterComponent } from './landing-page/layout/footer/footer.component';
import { JwtInterceptorService } from './shared-module/services/jwt-interceptor.service';
import { StoreModule } from '@ngrx/store';
import { userReducer } from './store/user.reducer';
import { EffectsModule } from '@ngrx/effects';
import { UserEffects } from './store/user.effect';
import { sharedReducer } from './store/shared/shared.reducer';
import { environment } from 'src/environments/environment';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

export class MyHammerConfig extends HammerGestureConfig {
  overrides = <any>{
    swipe: { direction: Hammer.DIRECTION_ALL },
  };
}
export function getApiBaseUrl() {
  if (environment.production) {
    return window.location.protocol + '//' + window.location.hostname;
  }
  return null;
}
@NgModule({
  declarations: [
    AppComponent,
    SettingsComponent,
    SaveBeforeDialogComponent,
    LoginComponent,
    RegistrationComponent,
    LayoutComponent,
    HeaderComponent,
    FooterComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    HammerModule,
    HttpClientModule,
    ReactiveFormsModule,
    StoreModule.forRoot({ user: userReducer, shared: sharedReducer }),
    EffectsModule.forRoot([UserEffects]),
    StoreDevtoolsModule.instrument({
      maxAge: 25, // Retains last 25 states
      logOnly: environment.production, // Restrict extension to log-only mode
      autoPause: true, // Pauses recording actions and state changes when the extension window is not open
    }),
  ],
  providers: [
    ExamSearchClient,
    ExamRunClient,
    QuestionClient,
    LoginClient,
    UserRegistrationClient,
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: MyHammerConfig,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptorService,
      multi: true,
    },
    {
      provide: ErrorHandler,
      useClass: GlobalErrorHandlerService,
    },
    {
      provide: API_BASE_URL, // That's the token we defined previously
      useFactory: getApiBaseUrl, // That's the actual service itself
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
