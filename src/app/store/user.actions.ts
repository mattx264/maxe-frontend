import { createAction, props } from '@ngrx/store';
import { LoginViewModel, UserViewModel } from '../api-client';
export const login = createAction(
  '[Login Page] Login',
  props<{ credentials: LoginViewModel; returnUrl: string }>()
);
export const loginFailed = createAction(
  '[Login Page] Login Failed',
  props<{ isFailed: boolean }>()
);

export const addUser = createAction(
  '[User] Add User',
  props<{ user: UserViewModel }>()
);
