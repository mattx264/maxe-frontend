import { UserViewModel } from '../api-client';
import { SharedState } from './shared/shared.state';
import { userReducer } from './user.reducer';

export interface AppState {
  user: UserViewModel | null;
  shared: SharedState;
}
