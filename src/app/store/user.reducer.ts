import { createReducer, on, Action } from '@ngrx/store';
import { UserViewModel } from '../api-client';
import { addUser } from './user.actions';

export const initialState: UserViewModel = null;

export const userReducer = createReducer(
  initialState,
  on(addUser, (state, { user }) =>
  user),
);
