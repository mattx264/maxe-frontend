import { createSelector } from '@ngrx/store';
import { UserViewModel } from '../api-client';
import { AppState } from './app.state';
import { userReducer } from './user.reducer';

export const selectUser = createSelector(
  (state: AppState) => state.user,
  (user: UserViewModel) => user
);
