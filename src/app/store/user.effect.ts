import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { EMPTY, of } from 'rxjs';
import { catchError, exhaustMap, map, tap } from 'rxjs/operators';
import { LoginClient } from '../api-client';
import { AuthService } from '../auth/services/auth.service';
import { setErrorMessage } from './shared/shared.actions';
import { addUser, login, loginFailed } from './user.actions';
@Injectable({
  providedIn: 'root',
})
export class UserEffects {
  userLogin$ = createEffect(() =>
    this.actions$.pipe(
      ofType(login),
      exhaustMap((action) =>
        this.loginClient.createToken(action.credentials).pipe(
          map((user) => addUser({ user })),
          tap((user) => this.authService.setsCurrentUser(user.user)),
          tap(() => this.router.navigate(['/'])),
          catchError((error) => of(setErrorMessage({ errorMessage: 'Login Failed' })))
        )
      )
    )
  );
  constructor(
    private actions$: Actions,
    private authService: AuthService,
    private loginClient: LoginClient,
    private router: Router
  ) {}
}

// userLogin$ = createEffect(()=>this.actions$.pipe(
//   ofType('[Login Page] Login'),
//    mergeMap(()=>this.loginClient.createToken()
//   .pipe(
//     map(user=>({type: '[User API] Login Success', payload: user})),
//     catchError(()=>EMPTY)
//   ))
// )
