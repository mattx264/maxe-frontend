export interface SharedState {
  showLoading: boolean;
  errorMessage: string | undefined;
}

export const initialState: SharedState = {
  showLoading: false,
  errorMessage: undefined,
};
