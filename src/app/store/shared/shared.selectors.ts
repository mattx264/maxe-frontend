import { createSelector } from '@ngrx/store';
import { AppState } from '../app.state';
import { SharedState } from './shared.state';

export const errorMessageSelector = createSelector(
  (state: AppState) => state.shared,
  (shared: SharedState) => shared
);
