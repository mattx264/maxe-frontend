import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { ExamSearchClient, ExamRunClient, QuestionClient } from 'src/app/api-client';
import { PresenterModule } from '../presenter.module';

import { ResultPresenterComponent } from './result-presenter.component';

describe('ResultPresenterComponent', () => {
  let component: ResultPresenterComponent;
  let fixture: ComponentFixture<ResultPresenterComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [RouterTestingModule,HttpClientModule],
        providers: [
          ExamSearchClient, ExamRunClient, QuestionClient,
          {
            provide: ActivatedRoute,
            useValue: { snapshot: { params: { guid: '1' } } },
          },
        ],
        declarations: [],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultPresenterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
