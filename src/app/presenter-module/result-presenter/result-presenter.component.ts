import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ExamResultViewModel } from 'src/app/api-client';
import { LayoutService } from 'src/app/shared-module/services/layout.service';
import { ProgressQuestionService } from 'src/app/shared-module/services/progress-question.service';

@Component({
  selector: 'app-result-presenter',
  templateUrl: './result-presenter.component.html',
  styleUrls: ['./result-presenter.component.scss'],
})
export class ResultPresenterComponent implements OnInit {
  guid: string;
  examResults: ExamResultViewModel[] = [];
  percentage: number;
  constructor(
    route: ActivatedRoute,
    private progressQuestionService: ProgressQuestionService,
    private layoutService: LayoutService
  ) {
    this.guid = route.snapshot.params.guid;
  }

  async ngOnInit(): Promise<void> {
    this.examResults = await this.progressQuestionService.getResult(this.guid);
    let correctAnswerCount = 0;
    this.examResults.forEach((element) => {
      const isCorrect = element.userAnswer === element.correctAnswers[0];
      if (isCorrect) {
        correctAnswerCount++;
      }
    });
    this.percentage = correctAnswerCount / this.examResults.length;
    this.layoutService.setBackButtonToRoot();
  }
}
