import { Component, OnInit } from '@angular/core';
import { QuestionService } from '../services/question.service';
import { ExamSearchViewModel } from '../../api-client';

@Component({
  selector: 'app-search-exam',
  templateUrl: './search-exam.component.html',
  styleUrls: ['./search-exam.component.scss'],
})
export class SearchExamComponent implements OnInit {
  exams: ExamSearchViewModel[] | null = null;
  constructor(private questionService: QuestionService) {}

  async ngOnInit(): Promise<void> {
    this.exams = await this.questionService.getSearchExams().toPromise();
  }
  onTitleClick(id: number) {}
}
