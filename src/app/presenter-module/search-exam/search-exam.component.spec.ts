import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ExamSearchClient } from 'src/app/api-client';

import { SearchExamComponent } from './search-exam.component';

describe('SearchExamComponent', () => {
  let component: SearchExamComponent;
  let fixture: ComponentFixture<SearchExamComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [SearchExamComponent],
        providers: [ExamSearchClient],
        imports:[HttpClientModule]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchExamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
