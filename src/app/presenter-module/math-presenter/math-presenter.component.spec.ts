import { ComponentFixture, TestBed } from '@angular/core/testing';
import { QuestionSessionViewModelQA } from 'src/app/test-common/question-session-view-models';

import { MathPresenterComponent } from './math-presenter.component';

describe('MathPresenterComponent', () => {
  let component: MathPresenterComponent;
  let fixture: ComponentFixture<MathPresenterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MathPresenterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MathPresenterComponent);
    component = fixture.componentInstance;
    component.currentQuestion = QuestionSessionViewModelQA.getQA()[0];

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
