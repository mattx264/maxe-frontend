import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { QuestionSessionViewModel } from 'src/app/api-client';

@Component({
  selector: 'app-math-presenter',
  templateUrl: './math-presenter.component.html',
  styleUrls: ['./math-presenter.component.scss']
})
export class MathPresenterComponent implements OnInit {
  @Input()
  currentQuestion: QuestionSessionViewModel;
  @Input()
  readOnlyValue: string;
  @Output()
  answerEvent: EventEmitter<string> = new EventEmitter<string>();
  constructor() { }

  ngOnInit(): void {
    //todo - remove this command after this will be set with backend example
    // before was  this.readOnlyValue = this.readOnlyValue || this.currentQuestion.answers[1].value;

    this.readOnlyValue = this.readOnlyValue || this.currentQuestion.answers[0].value;
  }

}
