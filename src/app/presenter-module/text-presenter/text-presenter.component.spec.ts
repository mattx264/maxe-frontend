import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TextPresenterComponent } from './text-presenter.component';

describe('TextPresenterComponent', () => {
  let component: TextPresenterComponent;
  let fixture: ComponentFixture<TextPresenterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TextPresenterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TextPresenterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
