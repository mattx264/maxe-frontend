import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { QuestionSessionViewModel } from 'src/app/api-client';

@Component({
  selector: 'app-text-presenter',
  templateUrl: './text-presenter.component.html',
  styleUrls: ['./text-presenter.component.scss']
})
export class TextPresenterComponent implements OnInit {
  @Input()
  currentQuestion: QuestionSessionViewModel;
  @Output()
  answerEvent: EventEmitter<string> = new EventEmitter<string>();
  answer: string;
  constructor() { }

  ngOnInit(): void {
  }
  done() {
    this.answerEvent.emit(this.answer);
  }
}
