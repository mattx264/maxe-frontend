import { ComponentFixture, TestBed } from '@angular/core/testing';
import { QuestionSessionViewModelQA } from 'src/app/test-common/question-session-view-models';

import { FillBlankPresenterComponent } from './fill-blank-presenter.component';

describe('FillBlankPresenterComponent', () => {
  let component: FillBlankPresenterComponent;
  let fixture: ComponentFixture<FillBlankPresenterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FillBlankPresenterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FillBlankPresenterComponent);
    component = fixture.componentInstance;
    component.currentQuestion = QuestionSessionViewModelQA.getFILL_BLANK()[0];

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
