import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ContentSectionAnswerType, QuestionSessionViewModel } from 'src/app/api-client';

@Component({
  selector: 'app-fill-blank-presenter',
  templateUrl: './fill-blank-presenter.component.html',
  styleUrls: ['./fill-blank-presenter.component.scss']
})
export class FillBlankPresenterComponent implements OnInit {
  @Input()
  currentQuestion: QuestionSessionViewModel;
  @Output()
  answerEvent: EventEmitter<string> = new EventEmitter<string>();
  charList: string[];
  ContentSectionAnswerType = ContentSectionAnswerType;

  constructor() { }

  ngOnInit(): void {
    let fillBlank = this.currentQuestion.answers.find(x => x.type == ContentSectionAnswerType.FILL_BLANK)
    this.charList = fillBlank.value.split('');
  }
  done() {
    this.answerEvent.emit('to nie jest skonczone');
  }
}
