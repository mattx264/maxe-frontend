import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexiblePresenterComponent } from './flexible-presenter/flexible-presenter.component';
import { PresenterRoutingModule } from './presenter-routing-module.';
import { ScreenCalibrateComponent } from './flexible-presenter/screen-calibrate/screen-calibrate.component';
import { MaterialModule } from '../shared-module/material-module';
import { SearchExamComponent } from './search-exam/search-exam.component';
import { ResultPresenterComponent } from './result-presenter/result-presenter.component';
import { StartupExamPresenterComponent } from './startup-exam-presenter/startup-exam-presenter.component';
import { QuizPresenterComponent } from './quiz-presenter/quiz-presenter.component';
import { SvgPresenterComponent } from './svg-presenter/svg-presenter.component';
import { InteractivePresenterComponent } from './interactive-presenter/interactive-presenter.component';
import { ListPresenterComponent } from './list-presenter/list-presenter.component';
import { QuickKnowledgePresenterComponent } from './quick-knowledge-presenter/quick-knowledge-presenter.component';
import { SharedModule } from '../shared-module/shared-module.module';
import { SectionPresenterComponent } from './shared/section-presenter/section-presenter.component';
import { SoundPresenterComponent } from './sound-presenter/sound-presenter.component';
import { DndListPresenterComponent } from './list-presenter/dnd-list-presenter/dnd-list-presenter.component';
import { TextListPresenterComponent } from './list-presenter/text-list-presenter/text-list-presenter.component';
import { CodePresenterComponent } from './code-presenter/code-presenter.component';
import { FillBlankPresenterComponent } from './fill-blank-presenter/fill-blank-presenter.component';
import { TextPresenterComponent } from './text-presenter/text-presenter.component';
import { FormsModule } from '@angular/forms';
import { MathPresenterComponent } from './math-presenter/math-presenter.component';
import { MathDirective } from './shared/math.directive';
import { MathModule } from './shared/math.module';

@NgModule({
  declarations: [
    FlexiblePresenterComponent,
    ScreenCalibrateComponent,
    SearchExamComponent,
    ResultPresenterComponent,
    StartupExamPresenterComponent,
    QuizPresenterComponent,
    SvgPresenterComponent,
    InteractivePresenterComponent,
    ListPresenterComponent,
    QuickKnowledgePresenterComponent,
    SectionPresenterComponent,
    SoundPresenterComponent,
    DndListPresenterComponent,
    TextListPresenterComponent,
    CodePresenterComponent,
    FillBlankPresenterComponent,
    TextPresenterComponent,
    MathPresenterComponent,
  ],
  imports: [
    CommonModule,
    PresenterRoutingModule,
    MaterialModule,
    SharedModule,
    //InlineSVGModule.forRoot(),
    FormsModule,
    MathModule.forRoot(),
  ],
})
export class PresenterModule {}
