import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ContentSectionAnswerType, QuestionSessionViewModel } from 'src/app/api-client';

@Component({
  selector: 'app-quiz-presenter',
  templateUrl: './quiz-presenter.component.html',
  styleUrls: ['./quiz-presenter.component.scss']
})
export class QuizPresenterComponent implements OnInit {
  @Input()
  currentQuestion: QuestionSessionViewModel;
  @Output()
  answerEvent: EventEmitter<string> = new EventEmitter<string>();

  ContentSectionAnswerType = ContentSectionAnswerType;
  constructor() { }

  ngOnInit(): void {
  }
  answerClick(answer: string) {
    this.answerEvent.emit(answer);
  }
}
