import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { QuestionSessionViewModelQA } from 'src/app/test-common/question-session-view-models';

import { QuizPresenterComponent } from './quiz-presenter.component';

describe('QuizPresenterComponent', () => {
  let component: QuizPresenterComponent;
  let fixture: ComponentFixture<QuizPresenterComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [QuizPresenterComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizPresenterComponent);
    component = fixture.componentInstance;

    component.currentQuestion = QuestionSessionViewModelQA.getQUIZ()[0];

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
