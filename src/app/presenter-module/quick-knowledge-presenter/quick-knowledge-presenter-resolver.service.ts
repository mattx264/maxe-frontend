import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { PresenterSettings, KeyValue } from '../../models/presenter-settings';

@Injectable({
  providedIn: 'root'
})
export class QuickKnowledgePresenterResolverService  {

  constructor() { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    //SERVERINFO - ant presenter settigs should be key value
    // settings table - id, type list, key - What is question level - values [user select, random, highes level, lowes level, mid level ]
    //
    var settings: PresenterSettings = {
      settings: [{ key: 'QUESTION_LEVEL', value: 'USER_SELECT' }]
    }
    return settings;
  }
}
