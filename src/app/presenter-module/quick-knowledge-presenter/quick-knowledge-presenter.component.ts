import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PresenterSettings } from 'src/app/models/presenter-settings';
import { QuickKnowledgePresenterDataService } from './quick-knowledge-presenter-data.service';
import { KeyValue } from '../../models/presenter-settings';

@Component({
  selector: 'app-quick-knowledge-presenter',
  templateUrl: './quick-knowledge-presenter.component.html',
  styleUrls: ['./quick-knowledge-presenter.component.scss']
})
export class QuickKnowledgePresenterComponent implements OnInit {
  settings: PresenterSettings;
  settingQuestionSelection: string;
  quickKnowledgeRunGuid: string;

  constructor(private route: ActivatedRoute, private quickKnowledgePresenterDataService: QuickKnowledgePresenterDataService) { }

  ngOnInit(): void {
    // 
    // I think we can do the same thing on presenter componet by setting you can applay to presenter
    //
    // var presenterSetting: PresenterSettings = this.route.snapshot.data.loadData;
   
    // this.settingQuestionSelection = presenterSetting.settings.find(x => x.key == "QUESTION_LEVEL").value;

    // this.quickKnowledgeRunGuid = this.route.snapshot.params.quickKnowledgeRunGuid;

    // this.quickKnowledgePresenterDataService.GetNextQuestion(this.quickKnowledgeRunGuid );
  }

}
