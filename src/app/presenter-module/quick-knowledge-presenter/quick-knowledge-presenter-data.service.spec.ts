import { TestBed } from '@angular/core/testing';

import { QuickKnowledgePresenterDataService } from './quick-knowledge-presenter-data.service';

describe('QuickKnowledgePresenterDataService', () => {
  let service: QuickKnowledgePresenterDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(QuickKnowledgePresenterDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
