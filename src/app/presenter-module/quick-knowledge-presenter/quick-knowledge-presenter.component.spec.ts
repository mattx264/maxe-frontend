import { CommonModule } from '@angular/common';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { MaterialModule } from 'src/app/shared-module/material-module';

import { QuickKnowledgePresenterComponent } from './quick-knowledge-presenter.component';

describe('QuickKnowledgePresenterComponent', () => {
  let component: QuickKnowledgePresenterComponent;
  let fixture: ComponentFixture<QuickKnowledgePresenterComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [MaterialModule, CommonModule, RouterTestingModule],
      declarations: [QuickKnowledgePresenterComponent],
      providers: [{
        provide: ActivatedRoute, useValue: {
          snapshot: {
            data: {
              loadData: {
                settings: [{ key: 'QUESTION_LEVEL', value: 'val' }]
              }
            },
            params:{
              quickKnowledgeRunGuid: 'quid',
            }
          }
        }
      }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuickKnowledgePresenterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
