import { TestBed } from '@angular/core/testing';

import { QuickKnowledgePresenterResolverService } from './quick-knowledge-presenter-resolver.service';

describe('QuickKnowledgePresenterResolverService', () => {
  let service: QuickKnowledgePresenterResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(QuickKnowledgePresenterResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
