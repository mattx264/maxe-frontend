import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { ContentSectionQuestionType, ExamResultViewModel, QuestionSectionViewModel } from 'src/app/api-client';

@Component({
  selector: 'app-section-presenter',
  templateUrl: './section-presenter.component.html',
  styleUrls: ['./section-presenter.component.scss']
})
export class SectionPresenterComponent implements OnInit {

  @Input()
  contentSection: QuestionSectionViewModel[];
  @Input() userAnswer:ExamResultViewModel;
  @Output()
  answerChangeEvent: EventEmitter<string> = new EventEmitter<string>();
  ContentSectionQuestionType = ContentSectionQuestionType;
  constructor() { }

  ngOnInit(): void {

  }
  answerFromChild(event) {
    this.answerChangeEvent.emit(event);
  }
}
