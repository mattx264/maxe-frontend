import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionPresenterComponent } from './section-presenter.component';

describe('SectionPresenterComponent', () => {
  let component: SectionPresenterComponent;
  let fixture: ComponentFixture<SectionPresenterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SectionPresenterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionPresenterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
