import { CommonModule } from '@angular/common';
import { ElementRef, Injectable } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { MathService } from '../services/math.service';
import { MathDirective } from './math.directive';
@Injectable()
export class MockElementRef {
  nativeElement: {};
}
describe('MathDirective', () => {
  it('should create an instance', () => {
    TestBed.configureTestingModule({
      providers: [{ provide: ElementRef, useClass: MockElementRef }],
    });
    const mathService = TestBed.inject(MathService);
    const directive = new MathDirective(
      mathService,
      TestBed.inject(ElementRef)
    );
    expect(directive).toBeTruthy();
  });
});
