import { Component, ElementRef, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ExamResultViewModel } from 'src/app/api-client';

@Component({
  selector: 'app-svg-presenter',
  templateUrl: './svg-presenter.component.html',
  styleUrls: ['./svg-presenter.component.scss'],
})
export class SvgPresenterComponent implements OnInit {
  @Input()
  svgPath: string;
  @Input() userAnswer: ExamResultViewModel;
  @Output()
  answerEvent: EventEmitter<string> = new EventEmitter<string>();
  constructor(private sanitizer: DomSanitizer,private elRef:ElementRef) {}

  ngOnInit(): void {
    // let answers = this.currentQuestion.answers;
    // if (answers.length > 1) {
    //   throw new Error("SVG presenter not support multi svg files");
    // }
    // this.trustedUrl = this.sanitizer.bypassSecurityTrustUrl(answers[0].value);
  }
  onLoad(event) {
    let svg: SVGSVGElement = this.elRef.nativeElement.querySelector('svg') as SVGSVGElement;
    svg.setAttribute('viewBox', `0 0 ${svg.clientWidth} ${svg.clientHeight}`);
    svg.setAttribute('preserveAspectRatio', 'none');
    if (this.userAnswer) {
      if (this.userAnswer.correctAnswers[0] == this.userAnswer.userAnswer) {
        this.elRef.nativeElement.querySelector(
          `svg #${this.userAnswer.userAnswer}`
        ).style.fill = 'green';
      } else {
        this.elRef.nativeElement.querySelector(
          `svg #${this.userAnswer.userAnswer}`
        ).style.fill = 'red';
        this.elRef.nativeElement.querySelector(
          `svg #${this.userAnswer.correctAnswers}`
        ).style.fill = 'yellow';
      }
    } else {
      var allElements = this.elRef.nativeElement.querySelectorAll('svg path'); //$("svg.us > *");
      allElements.forEach((element) => {
        element.addEventListener('click', (event: any) => {
          console.log(event);
          console.log(event.target.id);
          let id = event.target.id;
          this.answerEvent.emit(id);
        });
      });
    }
  }
}
