import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SvgPresenterComponent } from './svg-presenter.component';

describe('SvgPresenterComponent', () => {
  let component: SvgPresenterComponent;
  let fixture: ComponentFixture<SvgPresenterComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SvgPresenterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SvgPresenterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
