import { TestBed } from '@angular/core/testing';

import { InteractivePresenterGuard } from './interactive-presenter.guard';

describe('InteractivePresenterGuard', () => {
  let guard: InteractivePresenterGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(InteractivePresenterGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
