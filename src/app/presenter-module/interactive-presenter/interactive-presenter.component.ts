import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  AnswerType,
  QuestionSessionViewModel,
} from 'src/app/api-client';
import { ProgressQuestionService } from 'src/app/shared-module/services/progress-question.service';

@Component({
  selector: 'app-interactive-presenter',
  templateUrl: './interactive-presenter.component.html',
  styleUrls: ['./interactive-presenter.component.scss'],
})
export class InteractivePresenterComponent implements OnInit {
  guid: string;
  examMode: { repeatQuestionEvery: number } = { repeatQuestionEvery: 0 };
  currentQuestion: QuestionSessionViewModel;
  AnswerType = AnswerType;
  constructor(
    route: ActivatedRoute,
    private progressQuestionService: ProgressQuestionService
  ) {
    this.guid = route.snapshot.params.learnRunGuid;
  }

  async ngOnInit(): Promise<void> {
    let optionExamModel = await this.progressQuestionService.getOptionExamModel(
      this.guid
    );
    if (optionExamModel?.questionRepeat) {
      this.examMode.repeatQuestionEvery = optionExamModel.questionRepeat;
    }
    this.currentQuestion =
      await this.progressQuestionService.getCurrentQuestion(this.guid);
    this.isAllQuestionAnswerd();
  }
  async answerChange(answer) {
    this.currentQuestion = null;
    await this.progressQuestionService.setAnswer(
      this.guid,
      answer.value || answer
    );
    //TODO show answer
    //if(this.examMode.showAnswer)
    this.currentQuestion = await this.progressQuestionService.getNextQuestion(
      this.guid
    );
    this.isAllQuestionAnswerd();
  }

  private isAllQuestionAnswerd() {
    if (
      this.currentQuestion == null ||
      Object.keys(this.currentQuestion).length === 0
    ) {
      this.progressQuestionService.goToResult(this.guid);
    }
  }
}
