import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { ExamRunClient, ExamSearchClient, QuestionClient, QuestionSessionViewModel } from 'src/app/api-client';
import { MaterialModule } from 'src/app/shared-module/material-module';
import { ProgressQuestionService } from 'src/app/shared-module/services/progress-question.service';
import { QuestionSessionViewModelQA } from 'src/app/test-common/question-session-view-models';

import { InteractivePresenterComponent } from './interactive-presenter.component';

describe('InteractivePresenterComponent', () => {
  let component: InteractivePresenterComponent;
  let fixture: ComponentFixture<InteractivePresenterComponent>;
  let progressQuestionService: ProgressQuestionService;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [MaterialModule, CommonModule, RouterTestingModule,HttpClientModule],
        providers: [
          ExamSearchClient,
          QuestionClient,
          ExamRunClient,
          {
            provide: ActivatedRoute,
            useValue: { snapshot: { params: { id: '1' } } },
          },
        ],
        declarations: [InteractivePresenterComponent],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    progressQuestionService = TestBed.inject(ProgressQuestionService);

    let learnRunViewModels: QuestionSessionViewModel[] =
      QuestionSessionViewModelQA.getQA();
    let questionIndex = 0;
    spyOn(progressQuestionService, 'getCurrentQuestion').and.returnValue(
      Promise.resolve(learnRunViewModels[questionIndex])
    );
    spyOn(progressQuestionService, 'getNextQuestion').and.callFake((guid:string) => {
      return Promise.resolve(learnRunViewModels[++questionIndex]);
    });
    fixture = TestBed.createComponent(InteractivePresenterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
