import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ExamSearchClient, ExamSearchViewModel } from 'src/app/api-client';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {



  //#endregion
  constructor(private examSearchClient: ExamSearchClient) { }
  //#region methods
  // getExam(id: number): ExamModel {

  //   let x = ServerData.exams.find(x => x.id == id);
  //   return {
  //     id: x.id, title: x.title, imagePath: x.imagePath, questionList:
  //       x.questionList.map<QuestionModel>(q => {
  //         let answer = q.answerList.find(x => x.difficultyLevel == "Intermediate");
  //         if (answer == null) {
  //           answer = q.answerList.find(x => x.difficultyLevel == "Normal");
  //         }
  //         // this logic should be in back end - front end should only get what it needs
  //         let other = []
  //         if (answer.wrongAnswers == null) {
  //           other = [{ type: answer.type, subType: answer.subType, value: answer.value }];
  //         } else if (answer.wrongAnswers != null) {
  //           other = answer.wrongAnswers;
  //         }
  //         let answerResult = [q.correctAnswer];
  //         if (other != null) {
  //           answerResult = answerResult.concat(other)
  //         }
  //         return {
  //           id: q.id,
  //           questionSections: q.questionSections,
  //           answers: answerResult,
  //           type: answer.type,
  //           subType: answer.subType
  //         }
  //       })
  //   }

  // };
  getSearchExams(): Observable<ExamSearchViewModel[]> {
    return this.examSearchClient.getAll();
  }
  getSearchExam(id: number): Observable<ExamSearchViewModel> {
    return this.examSearchClient.getById(id);
  }
  // getExams(): ExamModel[] {
  //   let result = ServerData.exams.map<ExamModel>(x => {
  //     return this.getExam(x.id);
  //   });
  //   return result;
  // }
  // getQuestionById(questionId: number): QuestionModel {
  //   let exams = this.getExams();
  //   for (let i = 0; i < exams.length; i++) {
  //     const element = exams[i];
  //     let question = element.questionList.find(q => q.id === questionId);
  //     if (question != null) {
  //       return question;
  //     }
  //   };
  //   return null;
  // }
  //#endregion
}
