import { TestBed } from '@angular/core/testing';

import { PresenterService } from './presenter-service';
import { LearnRunModel } from '../../models/optionExamModel';
import { SaveExamRunModel } from '../../models/SaveExamRunModel';

describe('PresenterServiceService', () => {
  let service: PresenterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PresenterService);
    localStorage.clear();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  
});
