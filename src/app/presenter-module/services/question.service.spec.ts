import { TestBed } from '@angular/core/testing';

import { QuestionService } from './question.service';
import { ProgressQuestionService } from 'src/app/shared-module/services/progress-question.service';
import {
  ExamRunClient,
  ExamSearchClient,
  QuestionClient,
} from 'src/app/api-client';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';

describe('QuestionService', () => {
  let service: QuestionService;
  let progressQuestionService: ProgressQuestionService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, RouterTestingModule],
      providers: [ExamSearchClient, ExamRunClient, QuestionClient],
    });
    progressQuestionService = TestBed.inject(ProgressQuestionService);
    service = TestBed.inject(QuestionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  // skip question have to be rethink
  // it('should get exam with skip question', () => {
  //   spyOn(progressQuestionService, 'getSkip').and.returnValue([
  //     { howManyDays: 10, setDate: new Date(), questionId: 1 }
  //   ]);
  //   var exams = service.getExam(1);
  //   let isIdOneFind = exams.questionList.find(x => x.id == 1);
  //   expect(progressQuestionService.getSkip).toHaveBeenCalled();

  //   expect(isIdOneFind).toBeUndefined();
  // });
});
