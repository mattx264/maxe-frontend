import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class StartupExamPresenterService {

  constructor(private router: Router) { }

  startFlexExam(learnRunGuid: string, title: string) {
    this.router.navigate(['/home/flexible', title, learnRunGuid])
  }
  startExam(learnRunGuid: string, title: string) {
    this.router.navigate(['/home/interactive', title, learnRunGuid])
  }
}
