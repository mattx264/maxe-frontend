import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { StartupExamPresenterComponent } from './startup-exam-presenter.component';
import { RouterTestingModule } from '@angular/router/testing';
import { QuestionService } from '../services/question.service';
import { ExamModel } from 'src/app/models/examModel';
import { ExamRunClient, ExamSearchClient, QuestionClient } from 'src/app/api-client';
import { HttpClientModule } from '@angular/common/http';

describe('StartupExamPresenterComponent', () => {
  let component: StartupExamPresenterComponent;
  let fixture: ComponentFixture<StartupExamPresenterComponent>;
  let questionService: QuestionService;
  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [StartupExamPresenterComponent],
        imports: [RouterTestingModule, HttpClientModule],
        providers: [ExamSearchClient, ExamRunClient,QuestionClient],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    questionService = TestBed.inject(QuestionService);
    let examModel: ExamModel = {
      id: 1,
      questionList: [],
      title: 'test ',
      imagePath: '',
    };
    spyOn(questionService, 'getExam').and.returnValue(examModel);
    fixture = TestBed.createComponent(StartupExamPresenterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
