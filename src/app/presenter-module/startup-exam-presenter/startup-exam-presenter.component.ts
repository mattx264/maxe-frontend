import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { QuestionService } from '../services/question.service';
import { StartupExamPresenterService } from './startup-exam-presenter.service';
import { ProgressQuestionService } from 'src/app/shared-module/services/progress-question.service';
import { ExamSearchViewModel, ExamTypeEnum } from 'src/app/api-client';
import { SaveExamRunViewModel } from '../../api-client';

@Component({
  selector: 'app-startup-exam-presenter',
  templateUrl: './startup-exam-presenter.component.html',
  styleUrls: ['./startup-exam-presenter.component.scss']
})
export class StartupExamPresenterComponent implements OnInit {
  exam: ExamSearchViewModel;
  panelOpenState;
  questionRepeat: string = "0";
  randomizeQuestions: string = "0";
  constructor(private route: ActivatedRoute,
    private startupExamPresenterService: StartupExamPresenterService,
    private questionService: QuestionService,
    private progressQuestionService: ProgressQuestionService) { }

  async ngOnInit(): Promise<void> {
    this.route.snapshot.params.title;
    let id = this.route.snapshot.params.id;
    this.exam = await this.questionService.getSearchExam(id).toPromise();

  }
  async onStartClick() {
    let saveExamRunModel: SaveExamRunViewModel = this.GetExamRunModel();
    //TODO add this option on UI
    saveExamRunModel.examType = ExamTypeEnum.Exam;

    const learnRunGuid = await this.progressQuestionService.saveLearnSession(saveExamRunModel);
    this.startupExamPresenterService.startExam(learnRunGuid, this.exam.title);
  }

  async onStartFlexClick() {
    let saveExamRunModel: SaveExamRunViewModel = this.GetExamRunModel();
    saveExamRunModel.examType = ExamTypeEnum.Learn;
    const learnRunGuid = await this.progressQuestionService.saveLearnSession(saveExamRunModel);
    this.startupExamPresenterService.startFlexExam(learnRunGuid, this.exam.title);
  }
  private GetExamRunModel() {
    let saveExamRunModel: SaveExamRunViewModel = new SaveExamRunViewModel();

    saveExamRunModel.examIds = [this.exam.id];
    saveExamRunModel.questionLimit = null;
    saveExamRunModel.questionRepeat = +this.questionRepeat;
    saveExamRunModel.randomizeQuestions = this.randomizeQuestions == '0' ? false : true;
    return saveExamRunModel;
  }
}
