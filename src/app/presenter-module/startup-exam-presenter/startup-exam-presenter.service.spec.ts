import { TestBed, waitForAsync } from '@angular/core/testing';

import { StartupExamPresenterService } from './startup-exam-presenter.service';
import { RouterTestingModule } from '@angular/router/testing';

describe('StartupExamPresenterService', () => {
  let service: StartupExamPresenterService;
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule]
    })
  }));
  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StartupExamPresenterService);
    localStorage.clear();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  
});
