export interface ConfigurationModel {
    configurationButtonModel: ConfigurationButtonModel[];
    configurationGestureModel: ConfigurationGestureModel[];
    configurationVoiceModel: ConfigurationVoiceModel[];

}

export interface ConfigurationButtonModel {
    name: string;
    x: number;
    y: number;
    action: Function;
}
export interface ConfigurationGestureModel {
    name: string;
    gesture: string;
    action: Function;
}export interface ConfigurationVoiceModel {
    name: string;
    text: string;
    action: Function;
}