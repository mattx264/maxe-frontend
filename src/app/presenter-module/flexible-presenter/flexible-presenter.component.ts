import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {
  ConfigurationButtonModel,
  ConfigurationModel,
} from '../models/confifurationModel';
import { SpeechSynthesisService } from '../../shared-module/services/speech-synthesis.service';
import { ConfigurationService } from '../../shared-module/services/configuration.service';
import { ProgressQuestionService } from 'src/app/shared-module/services/progress-question.service';
import { Subscription } from 'rxjs';
import { LoggingService } from '../../shared-module/services/logging.service';
import {
  AnswerViewModel,
  ContentSectionAnswerType,
  ContentSectionQuestionType,
  ExamResultViewModel,
  ExamRunViewModel,
  QuestionSectionViewModel,
  QuestionSessionViewModel,
} from 'src/app/api-client';

@Component({
  selector: 'app-flexible-presenter',
  templateUrl: './flexible-presenter.component.html',
  styleUrls: ['./flexible-presenter.component.scss'],
})
export class FlexiblePresenterComponent implements OnInit {
  currentQuestion: QuestionSessionViewModel;
  //currentQuestionIndex: number;
  configuration: ConfigurationModel;
  lastPlayed: string;
  //questionList: QuestionModel[];
  routerEventsSubscribe: Subscription;

  // repeat question every 2 question eg q1 ->q2 -> q1 ->q2 ->q3->q2
  // is mustMarkRemember: boolean - think is good idea ? if it is not option by default question has to be mark as rememberd
  examMode: { repeatQuestionEvery: number } = { repeatQuestionEvery: 0 };
  learnSessionGuid: string;
  correctAnswer: AnswerViewModel[];
  answers: string[];
  correctAnswerPlayIndex = 0;
  optionExamModel: ExamRunViewModel;

  constructor(
    private speechSynthesisService: SpeechSynthesisService,
    private progressQuestionService: ProgressQuestionService,
    private configurationService: ConfigurationService,
    private router: Router,
    private route: ActivatedRoute,
    private loggingService: LoggingService
  ) {
    this.configuration = configurationService.getConfiguration();
    if (this.configuration == null) {
      this.router.navigate(['/home/calibrate']);
      return;
    }
    this.learnSessionGuid = route.snapshot.params.learnRunGuid;
  }

  actionButtons: ConfigurationButtonModel[] = [];

  async ngOnInit(): Promise<void> {
    this.configButtons();
    this.optionExamModel =
      await this.progressQuestionService.getOptionExamModel(
        this.learnSessionGuid
      );

    if (this.optionExamModel?.questionRepeat) {
      this.examMode.repeatQuestionEvery = this.optionExamModel.questionRepeat;
    }
    this.currentQuestion =
      await this.progressQuestionService.getCurrentQuestion(
        this.learnSessionGuid
      );
  }

  async play() {
    this.loggingService.log('PLAY');
    if (this.isQuestionCompleted()) {
      this.currentQuestion = await this.getNextQuestion();
      if (this.currentQuestion == null) {
        this.progressQuestionService.goToResult(this.learnSessionGuid);
        return;
      }
    }
    if (this.currentQuestion.isQuestionPlayed) {
      if (this.correctAnswer == null) {
        this.correctAnswer =
          await this.progressQuestionService.getCurrentQuestionAnswer(
            this.learnSessionGuid
          );

        this.answers = this.correctAnswer.map((x) => x.value);
      }
      let text = this.answers[this.correctAnswerPlayIndex++];
      this.speechSynthesisService.speek(text, this.optionExamModel.language);
      this.lastPlayed = text;
      //todo this can be use for tip mechanizm
      // if (Array.isArray(correctAnswer)) {
      //   let text = this.getTextFromAnswerSections([this.currentQuestion.answers[++this.currentQuestion.answerIndex]]);
      //   this.speechSynthesisService.speek(text);
      //   this.lastPlayed = text;

      // }
    } else {
      var text: string = this.getTextFromQuestionSections(
        this.currentQuestion.questionSections
      );
      this.speechSynthesisService.speek(text, this.optionExamModel.language);
      this.lastPlayed = text;
      this.currentQuestion.isQuestionPlayed = true;
      this.correctAnswer = null;
      this.correctAnswerPlayIndex = 0;
      // if (Array.isArray(this.currentQuestion.answers)) {
      //   if (this.currentQuestion.answerIndex == null) {
      //     this.currentQuestion.answerIndex = -1;
      //   }
      // }
    }
  }
  getTextFromQuestionSections(
    questionSections: QuestionSectionViewModel[]
  ): string {
    //TODO - have to implement convert for html, code and math
    let output = '';
    questionSections.forEach((element) => {
      switch (element.type) {
        case ContentSectionQuestionType.TEXT:
          output += element.value;
          break;
        default:
      }
    });
    return output;
  }
  getTextFromAnswerSections(questionSections: AnswerViewModel[]): string {
    //TODO - have to implement convert for html, code and math
    let output = '';
    questionSections.forEach((element) => {
      if (element.type == ContentSectionAnswerType.TEXT) {
      }
      switch (element.type) {
        case ContentSectionAnswerType.TEXT:
          output += element.value;
          break;
        default:
      }
    });
    return output;
  }
  isQuestionCompleted(): boolean {
    if (this.currentQuestion.isQuestionPlayed === false) {
      return false;
    }
    if (this.correctAnswer != null) {
      if (this.answers.length === this.correctAnswerPlayIndex) {
        return true;
      }
    }
    return false;
    // todo this can be use for tip mechanizm
    // if ((this.currentQuestion.answerIndex + 1) === this.currentQuestion.answers.length) {
    //   return true;
    // }
  }
  repeat() {
    this.loggingService.log('REPEAT');

    if (this.lastPlayed == null || this.lastPlayed == '') {
      this.speechSynthesisService.speek(
        'First click play',
        this.optionExamModel.language
      );

      return;
    }
    this.speechSynthesisService.speek(
      this.lastPlayed,
      this.optionExamModel.language
    );
  }
  skip() {
    throw new Error('NOT IMPLEMENTED');
    // Question is what skip should do ?
    // skip question in current learn session
    // or skip is for some time when you come back ?
    // this.loggingService.log("SKIP");

    // this.progressQuestionService.setSkip(
    //   {
    //     questionId: this.currentQuestion.id,
    //     setDate: new Date(),
    //     howManyDays: 10
    //   }
    // );
  }
  async remember() {
    this.loggingService.log('REMEMBER');

    this.currentQuestion = this.progressQuestionService.markQuestionRemember(
      this.currentQuestion,
      this.learnSessionGuid
    );
    this.currentQuestion = await this.getNextQuestion();
    this.play();
  }
  async getNextQuestion(): Promise<QuestionSessionViewModel> {
    //this.correctAnswerPlayIndex = 0;
    this.correctAnswer = null;
    const currentQuestion = await this.progressQuestionService.getNextQuestion(
      this.learnSessionGuid
    );
    if (currentQuestion == null || currentQuestion.questionSections == null) {
      return null;
    }
    this.configButtons();
    return currentQuestion;
  }

  /* getNextRepeatQuestion(repeatQuestionEvery: number): QuestionModel {
     if (this.currectQuestion == null) {
       return this.questionList[++this.currectQuestionIndex];
     }
     console.log(this.questionList)
     let lastIndexOfRememberdQuestion = this.questionList
       .slice()
       .reverse()
       .findIndex(x => x.isMarkAsRemembered);
       lastIndexOfRememberdQuestion = lastIndexOfRememberdQuestion >= 0 ? this.questionList.length - lastIndexOfRememberdQuestion : lastIndexOfRememberdQuestion;

     if ((this.currectQuestionIndex + 1 >= repeatQuestionEvery) && lastIndexOfRememberdQuestion == -1) {
       console.log("A" + lastIndexOfRememberdQuestion + " " + this.currectQuestionIndex)
       this.currectQuestionIndex = 0;
       return this.questionList[this.currectQuestionIndex];
       //            2                               1 + 2 =3
       //            3                               2 + 2 =4
     } else if (this.currectQuestionIndex >= (lastIndexOfRememberdQuestion + repeatQuestionEvery -1) && lastIndexOfRememberdQuestion !== -1) {
       this.currectQuestionIndex = lastIndexOfRememberdQuestion ;
       console.log("Increase index to: " + lastIndexOfRememberdQuestion + " " + this.currectQuestionIndex)

       return this.questionList[lastIndexOfRememberdQuestion];
     }
     else {
       console.log("C " + lastIndexOfRememberdQuestion + " " + this.currectQuestionIndex)
       return this.questionList[++this.currectQuestionIndex];
     }

   }*/
  // setQuestionIndex(index: number) {
  //   this.currentQuestionIndex = +index - 1;
  //   this.getNextQuestion();
  // }
  execFunction(func: Function) {
    func.bind(this).call();
  }

  configButtons() {
    this.actionButtons = [];

    // switch (this.currentQuestion.type) { // Concept of question type is not apply to learn session
    //   case "QA":
    this.actionButtons.push(
      this.configuration.configurationButtonModel.find((x) => x.name === 'Play')
    );
    this.actionButtons[this.actionButtons.length - 1].action = this.play;
    this.actionButtons.push(
      this.configuration.configurationButtonModel.find(
        (x) => x.name === 'Repeat'
      )
    );
    this.actionButtons[this.actionButtons.length - 1].action = this.repeat;
    this.actionButtons.push(
      this.configuration.configurationButtonModel.find((x) => x.name === 'Skip')
    );
    this.actionButtons[this.actionButtons.length - 1].action = this.skip;
    this.actionButtons.push(
      this.configuration.configurationButtonModel.find(
        (x) => x.name === 'Remember'
      )
    );
    this.actionButtons[this.actionButtons.length - 1].action = this.remember;
    //     break;
    // }
  }
}
