import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ScreenCalibrateComponent } from './screen-calibrate.component';
import { RouterTestingModule } from '@angular/router/testing';
import { ConfigurationService } from '../../../shared-module/services/configuration.service';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../../../shared-module/material-module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { Router } from '@angular/router';

describe('ScreenCalibrateComponent', () => {
  let component: ScreenCalibrateComponent;
  let fixture: ComponentFixture<ScreenCalibrateComponent>;
  let configurationService: ConfigurationService;
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ScreenCalibrateComponent],
      providers: [ConfigurationService],
      imports: [RouterTestingModule, CommonModule, MaterialModule, NoopAnimationsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    configurationService = TestBed.inject(ConfigurationService);
    fixture = TestBed.createComponent(ScreenCalibrateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should click', () => {
    spyOn(configurationService, 'setConfiguration');
    component.camptureClick = true;
    let event = new MouseEvent('mouseup', {
      bubbles: true, // only bubbles and cancelable
      cancelable: true, // work in the Event constructor
      clientX: 100,
      clientY: 200
    });
    document.body.dispatchEvent(event);
    expect(component.actionButtons.length).toBe(1);
    expect(component.actionButtons[0].x).toBe(72);
    expect(component.actionButtons[0].y).toBe(172);

    expect(configurationService.setConfiguration).not.toHaveBeenCalled();
  });
  it('should set all buttons', () => {
    component.currentButtonIndex = component.buttonList.length - 1;
    spyOn(configurationService, 'setConfiguration');
    spyOn(component, 'continue');
    component.camptureClick = true;
    let event = new MouseEvent('mouseup', {
      bubbles: true, // only bubbles and cancelable
      cancelable: true, // work in the Event constructor
      clientX: 100,
      clientY: 200
    });
    document.body.dispatchEvent(event);
    expect(component.continue).toHaveBeenCalled();
    expect(component.camptureClick).toBeFalse();
    expect(component.currentButtonIndex).toBe(0);
  });
  it('should not continue, non option was selected', () => {
    component.continue();
    expect(component.step).toBe(1);
  });

  it('should continue step 2', () => {
    component.screenGesture = true;
    component.continue();
    expect(component.step).toBe(2);
  });

  it('should continue step 3', () => {
    component.screenClick = true;
    component.continue();
    expect(component.step).toBe(3);
  });
  it('should continue to main page', () => {
    const router = TestBed.get(Router);
    spyOn(router, "navigate");
    component.actionGesture = [{}];
    component.actionButtons = [{}];

    component.screenClick = true;
    component.screenGesture = true;
    component.continue();
    expect(router.navigate).toHaveBeenCalled();
  });

  it('should swipe be ignore ', () => {
    spyOn(component, 'gestureSet');
    component.onGesture({});
    expect(component.gestureSet).not.toHaveBeenCalled();
  });
  it('should swipe set first and ignore second ', () => {
    component.step = 2;
    spyOn(component, 'gestureSet');
    component.selectedGesture = "Swipe Right";
    component.onGesture({ type: "swiperight" });
    expect(component.gestureSet).toHaveBeenCalled();
    component.onGesture({ type: "swiperight" });
    expect(component.selectedGesture).toBeNull()

  });
  it('should swipe all direction ', () => {
    component.step = 2;
    spyOn(component, 'gestureSet');
    component.selectedGesture = "Swipe Left";
    component.onGesture({ type: "swipeleft" });
    component.onGesture({ type: "swipeleft" });
    expect(component.selectedGesture).toBeNull();
    component.selectedGesture = "Swipe Up";
    component.onGesture({ type: "swipeup" });
    component.onGesture({ type: "swipeup" });
    expect(component.selectedGesture).toBeNull()
    component.selectedGesture = "Swipe Down";
    component.onGesture({ type: "swipedown" });
    component.onGesture({ type: "swipedown" });
    expect(component.selectedGesture).toBeNull()

  });
  it('should gestureSet', () => {
    component.gestureSet('swiperight');
    expect(component.actionGesture.length).toBe(1);
  });
  it('should gestureSet continue', () => {
    spyOn(component, 'continue');
    component.currentButtonIndex = component.buttonList.length - 1;
    component.gestureSet('swiperight');
    expect(component.continue).toHaveBeenCalled();
    expect(component.currentButtonIndex).toBe(0);
  });
});
