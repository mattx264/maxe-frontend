import { Component, OnInit, HostListener, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { ConfigurationButtonModel, ConfigurationGestureModel, ConfigurationModel } from '../../models/confifurationModel';
import { ConfigurationService } from '../../../shared-module/services/configuration.service';
import { MatLegacySnackBar as MatSnackBar } from '@angular/material/legacy-snack-bar';
import { SpeechRecognitionService } from '../../../shared-module/services/speech-recognition.service';
@Component({
  selector: 'app-screen-calibrate',
  templateUrl: './screen-calibrate.component.html',
  styleUrls: ['./screen-calibrate.component.scss']
})
export class ScreenCalibrateComponent implements OnInit {
  step = 1;
  buttonSizeCenter: number = 56 / 2;
  screenClick = false;
  screenGesture = false;
  voiceControl = false;
  camptureClick = false;
  selectedGesture;
  buttonList = [
    "Play",
    "Repeat",
    "Skip",
    "Remember"
    // 'A',
    // 'B',
    // 'C',
    // 'D',
    // 'Next'
  ];
  gestureList = [
    "Swipe Right",
    "Swipe Left",
    "Swipe Up",
    "Swipe Down",
    "Tap once",
    "Tap twice"
  ]
  currentButtonIndex = 0;
  actionButtons: any[] = [];
  actionGesture: any[] = [];

  configurationModel: ConfigurationModel = {
    configurationButtonModel: [],
    configurationGestureModel: [],
    configurationVoiceModel: []
  }
  speechText: any;
  isVoiceRecorderActive: boolean;
  isSpeechSuppoerByBrowser: boolean;
  constructor(
    private configurationService: ConfigurationService,
    private router: Router,
    private snackBar: MatSnackBar,
    private speechRecognitionService: SpeechRecognitionService
    , private changeDetectorRef: ChangeDetectorRef) {
    this.isSpeechSuppoerByBrowser = speechRecognitionService.isSpeechSuppoerByBrowser;

  }

  ngOnInit(): void {

  }
  continue() {
    if (!this.screenGesture && !this.screenClick && !this.voiceControl) {
      this.snackBar.open("You have to select at least one control", '', {
        duration: 3000,
        horizontalPosition: 'center',
        verticalPosition: 'top',
      });
      return;
    }
    else if (this.screenGesture && this.actionGesture.length === 0) {
      this.step = 2;
      return;
    }
    else if (this.screenClick && this.actionButtons.length === 0) {
      this.camptureClick = true;
      this.step = 3;
      return;
    }
    else if (this.voiceControl && this.actionButtons.length === 0) {
      this.step = 4;
      return;
    }
    this.router.navigate(['/']);
  }
  //#region click
  @HostListener('document:mouseup', ['$event'])
  clickout(event) {
    if (!this.camptureClick) {
      return;
    }
    const configurationButtonModel: ConfigurationButtonModel = {
      name: this.buttonList[this.currentButtonIndex],
      x: event.x - this.buttonSizeCenter,
      y: event.y - this.buttonSizeCenter,
      action: null
    }
    this.actionButtons.push(configurationButtonModel);
    //this.configurationService.addConfiguration(configurationModel);
    this.currentButtonIndex++;

    if (this.buttonList.length === this.currentButtonIndex) {
      this.configurationModel.configurationButtonModel = this.actionButtons;
      this.configurationService.setConfiguration(this.configurationModel);
      this.camptureClick = false;
      this.currentButtonIndex = 0;
      this.continue();
    }
  };
  //#endregion // click

  //#region gesture
  gestureSet(gesture: string) {
    const configurationModel: ConfigurationGestureModel = {
      name: this.buttonList[this.currentButtonIndex],
      gesture: gesture,
      action: null
    };
    this.actionGesture.push(configurationModel);
    this.currentButtonIndex++;
    if (this.buttonList.length === this.currentButtonIndex) {
      this.configurationModel.configurationGestureModel = this.actionGesture;
      this.configurationService.setConfiguration(this.configurationModel);
      this.currentButtonIndex = 0;
      this.continue();
    }
  }
  onGesture($event) {
    if (this.selectedGesture == null) {
      return;
    }
    switch (this.selectedGesture) {
      case "Swipe Right":
        if ($event.type == "swiperight") {
          this.gestureSet("swiperight");
          this.selectedGesture = null;
        }
        break;
      case "Swipe Left":
        if ($event.type == "swipeleft") {
          this.gestureSet("swipeleft");
          this.selectedGesture = null;
        }
        break;
      case "Swipe Up":
        if ($event.type == "swipeup") {
          this.gestureSet("swipeup");
          this.selectedGesture = null;
        }
        break;
      case "Swipe Down":
        if ($event.type == "swipedown") {
          this.gestureSet("swipedown");
          this.selectedGesture = null;
        }
        break;
      case "Tap once":
        if ($event.tapCount == 1) {
          this.gestureSet("tapCount1");
          this.selectedGesture = null;
        }
        break;
      case "Tap twice":
        if ($event.tapCount == 2) {
          this.gestureSet("tapCount2");
          this.selectedGesture = null;
        }
        break;
    }
  }
  onGestureChange($event) {
    this.selectedGesture = $event.value;
  }
  //#endregion //gesture
  //#region voice
  onListen($event) {
    // this.speechRecognitionService.startListen();
    // this.isVoiceRecorderActive = true;
    // this.speechRecognitionService.onResult.subscribe(text => {
    //   this.speechText = text;
    //   this.speechRecognitionService.stopListen();
    //   this.isVoiceRecorderActive = false;

    //   this.changeDetectorRef.detectChanges();
    // });
  }
  //#endregion
}
