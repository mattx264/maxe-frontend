import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { FlexiblePresenterComponent } from './flexible-presenter.component';
import { MatLegacyDialog as MatDialog } from '@angular/material/legacy-dialog';
import { SaveBeforeDialogComponent } from '../../shared-module/componets/save-before-dialog/save-before-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class CanDeactivateGuard  {
  constructor(public dialog: MatDialog) {

  }
  canDeactivate(component: FlexiblePresenterComponent, currentRoute: ActivatedRouteSnapshot, currentState: RouterStateSnapshot, nextState?: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    if (component.configuration == null || component.currentQuestion == null) {
      return true;
    }
    return new Promise<boolean>(async (resolve, reject) => {
      var result = await this.openDialog();
      resolve(result);
    });
  }
  openDialog(): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      const dialogRef = this.dialog.open(SaveBeforeDialogComponent);

      dialogRef.afterClosed().subscribe(result => {
        console.log(`Dialog result: ${result}`);
        resolve(result);
      });
    });
  }

}
