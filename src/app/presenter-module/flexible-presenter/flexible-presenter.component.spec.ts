
//FIX TEST 

/*import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { FlexiblePresenterComponent } from './flexible-presenter.component';
import { AppModule } from '../../app.module';
import { RouterTestingModule } from '@angular/router/testing';
import { ConfigurationButtonModel, ConfigurationModel } from '../models/confifurationModel';
import { SpeechSynthesisService } from 'src/app/shared-module/services/speech-synthesis.service';
import { ConfigurationService } from '../../shared-module/services/configuration.service';
import { ProgressQuestionService } from 'src/app/shared-module/services/progress-question.service';
import { MaterialModule } from '../../shared-module/material-module';
import { By, BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { PresenterModule } from '../presenter.module';
import { Component } from '@angular/core';
import { QuestionService } from '../services/question.service';
import { PresenterService } from '../services/presenter-service';
import { ActivatedRoute } from '@angular/router';
import { SaveExamRunModel } from 'src/app/models/SaveExamRunModel';
import { LearnnSessionViewModel } from '../../models/questionModel';
import { LearnRunViewModels } from '../../test-common/learn-session-view-models';

describe('FlexiblePresenterComponent', () => {
  let component: FlexiblePresenterComponent;
  let fixture: ComponentFixture<FlexiblePresenterComponent>;
  let progressQuestionService: ProgressQuestionService;
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [MaterialModule, CommonModule, RouterTestingModule],
      providers: [{
        provide: ActivatedRoute,
        useValue: { snapshot: { params: { 'id': '1' } } }
      }],
      declarations: [FlexiblePresenterComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    let speechSynthesisService = TestBed.get<SpeechSynthesisService>(SpeechSynthesisService);
    spyOn(speechSynthesisService, 'speek').and.callFake(() => {

    });
    const configurationModel: ConfigurationModel = {
      configurationGestureModel: [],
      configurationButtonModel: [
        { action: null, name: 'Play', x: 0, y: 0 },
        { action: null, name: 'Repeat', x: 0, y: 0 },
        { action: null, name: 'Skip', x: 0, y: 0 },
        { action: null, name: 'Remember', x: 0, y: 0 }
      ],
      configurationVoiceModel: []
    };
    progressQuestionService = TestBed.get(ProgressQuestionService);

    let learnRunViewModels: LearnnSessionViewModel[] = LearnRunViewModels.getQA();
    let questionIndex = 0;
    spyOn(progressQuestionService, 'getCurrentQuestion').and.returnValue(
      learnRunViewModels[questionIndex]
    );
    spyOn(progressQuestionService, 'getNextQuestion').and.callFake(() => {
      return learnRunViewModels[++questionIndex];
    });
    let configurationService = TestBed.get(ConfigurationService);
    spyOn(configurationService, 'getConfiguration').and.returnValue(configurationModel);

    fixture = TestBed.createComponent(FlexiblePresenterComponent);
    component = fixture.componentInstance;

    //progressQuestionService.saveLearnSession(questionModels);
    component.examMode.repeatQuestionEvery = 0;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();

  });

  it('should get next question', () => {
    component.getNextQuestion();
    expect(component.currentQuestion.questionSections[0].value).toBe("q2");
  });
  it('should play question', () => {
    component.play();
    expect(component.lastPlayed).toBe(component.currentQuestion.questionSections[0].value);
  });
  it('should play answer', () => {
    component.play();
    component.play();

    expect(component.lastPlayed).toBe(component.currentQuestion.answers[0]);
  });
  it('should play 2 question', () => {
    component.play();
    component.play();
    component.play();
    expect(component.currentQuestion.answerIndex).toBe(-1);
    expect(component.currentQuestion.questionSections[0].value).toBe("q2");
  });
  it('should play 2 answer', () => {
    component.play();//q1
    component.play();//a1
    component.play();//q2
    component.play();//a1 

    expect(component.currentQuestion.answerIndex).toBe(0);
    expect(component.currentQuestion.questionSections[0].value).toBe("q2");
    expect(component.lastPlayed).toBe("a1");

  });
  it('should play 2 answer 2', () => {
    component.play();//q1
    component.play();//a1
    component.play();//q2
    component.play();//a1
    component.play();//a2

    expect(component.lastPlayed).toBe("a2");
  });


  it('should do detail test with play', () => {
    expect(component.currentQuestion.isQuestionPlayed).toBeFalse();
    expect(component.currentQuestion.answerIndex).toBeUndefined();
    expect(component.currentQuestion.answers.length).toBe(1);

    component.play();    //question 1
    expect(component.currentQuestion.isQuestionPlayed).toBeTrue();
    expect(component.currentQuestion.answerIndex).toBe(-1);
    expect(component.currentQuestion.answers.length).toBe(1);
    component.play(); // answer 1
    expect(component.currentQuestion.isQuestionPlayed).toBeTrue();
    expect(component.currentQuestion.answerIndex).toBe(0);
    expect(component.currentQuestion.answers.length).toBe(1);

    component.play(); //question 2
    expect(component.currentQuestion.isQuestionPlayed).toBeTrue();
    expect(component.currentQuestion.answerIndex).toBe(-1);
    expect(component.currentQuestion.answers.length).toBe(2);
    component.play(); // answer 1
    expect(component.currentQuestion.isQuestionPlayed).toBeTrue();
    expect(component.currentQuestion.answerIndex).toBe(0);
    expect(component.currentQuestion.answers.length).toBe(2);
    component.play();  // answer 2

    expect(component.currentQuestion.isQuestionPlayed).toBeTrue();
    expect(component.currentQuestion.answerIndex).toBe(1);
    expect(component.currentQuestion.answers.length).toBe(2);

    component.play(); //q 3
    expect(component.currentQuestion.isQuestionPlayed).toBeTrue();
    expect(component.currentQuestion.answerIndex).toBe(-1);
    expect(component.currentQuestion.answers.length).toBe(3);

    component.play(); //q 3
    expect(component.currentQuestion.isQuestionPlayed).toBeTrue();
    expect(component.currentQuestion.answerIndex).toBe(0);
    expect(component.currentQuestion.answers.length).toBe(3);

    component.play(); //q 3
    expect(component.currentQuestion.isQuestionPlayed).toBeTrue();
    expect(component.currentQuestion.answerIndex).toBe(1);
    expect(component.currentQuestion.answers.length).toBe(3);

    component.play(); //q 3
    //this is last question, what to do next ? is question of future
  });

  it('should render action button', () => {
    var button = fixture.debugElement.query(By.css('.action-btn'));
    expect(button).toBeTruthy();
  });
});
*/