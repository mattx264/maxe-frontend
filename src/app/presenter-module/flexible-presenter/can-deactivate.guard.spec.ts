import { TestBed, waitForAsync } from '@angular/core/testing';

import { CanDeactivateGuard } from './can-deactivate.guard';
import { MaterialModule } from 'src/app/shared-module/material-module';
import { MatLegacyDialog as MatDialog, MatLegacyDialogRef as MatDialogRef } from '@angular/material/legacy-dialog';
import { of } from 'rxjs';
import { FlexiblePresenterComponent } from './flexible-presenter.component';
import { CommonModule } from '@angular/common';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRoute } from '@angular/router';
import { ExamRunClient, ExamSearchClient, QuestionClient } from 'src/app/api-client';
import { HttpClientModule } from '@angular/common/http';
import { Component } from '@angular/core';

describe('CanDeactivateGuard', () => {
  let guard: CanDeactivateGuard;
  let dialogSpy: jasmine.Spy;
  let dialogRefSpyObj = jasmine.createSpyObj({
    afterClosed: of({}),
    close: null,
  });
  dialogRefSpyObj.componentInstance = { body: '' }; // attach componentInstance to the spy object...
  @Component({ selector: 'calibrate', template: `` })
  class BlankComponent {}
  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [
          MaterialModule,
          CommonModule,
          RouterTestingModule.withRoutes(
            [{path: 'calibrate', component: BlankComponent}]
          ),
          HttpClientModule,
        ],
        providers: [
          ExamSearchClient,
          ExamRunClient,
          QuestionClient,
          {
            provide: ActivatedRoute,
            useValue: { snapshot: { params: { id: '1' } } },
          },
        ],
      });
    })
  );

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(CanDeactivateGuard);
    localStorage.clear();
  });
  beforeEach(() => {
    dialogSpy = spyOn(TestBed.get(MatDialog), 'open').and.returnValue(
      dialogRefSpyObj
    );

  });
  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
  it('should open dialog', () => {
    guard.openDialog();
    expect(dialogRefSpyObj.afterClosed).toHaveBeenCalled();
  });
  it('should deactivate', async () => {
    let fixture = TestBed.createComponent(FlexiblePresenterComponent);
    let component = fixture.componentInstance;
    component.configuration = {
      configurationButtonModel: [],
      configurationGestureModel: [],
      configurationVoiceModel: [],
    };
    spyOn(guard, 'openDialog').and.returnValue(
      new Promise((result) => result(false))
    );

    let routeMock: any = { snapshot: {} };
    let routeStateMock: any = { snapshot: {}, url: '/cookies' };
    let routerMock = { navigate: jasmine.createSpy('navigate') };

    let result = await guard.canDeactivate(
      component,
      routeMock,
      routeStateMock
    );
    expect(result).toBe(true);
  });
  it('should deactivate config null', async () => {
    let fixture = TestBed.createComponent(FlexiblePresenterComponent);
    let component = fixture.componentInstance;
    component.configuration = null;
    spyOn(guard, 'openDialog').and.returnValue(new Promise(result => true));
    let routeMock: any = { snapshot: {} };
    let routeStateMock: any = { snapshot: {}, url: '/calibrate' };
    //let routerMock = { navigate: jasmine.createSpy('navigate') };

    let result = await guard.canDeactivate(
      component,
      routeMock,
      routeStateMock
    );
    expect(result).toBe(true);
  });
});
