import { ComponentFixture, TestBed } from '@angular/core/testing';
import { QuestionSectionViewModel } from 'src/app/api-client';
import { QuestionSessionViewModelQA } from 'src/app/test-common/question-session-view-models';

import { TextListPresenterComponent } from './text-list-presenter.component';

describe('TextListPresenterComponent', () => {
  let component: TextListPresenterComponent;
  let fixture: ComponentFixture<TextListPresenterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TextListPresenterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TextListPresenterComponent);
    component = fixture.componentInstance;
    component.currentQuestion = QuestionSessionViewModelQA.getList()[0];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
