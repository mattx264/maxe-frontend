import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { QuestionSessionViewModel } from 'src/app/api-client';
import { ListPresenterService } from '../list-presenter.service';

@Component({
  selector: 'app-text-list-presenter',
  templateUrl: './text-list-presenter.component.html',
  styleUrls: ['./text-list-presenter.component.scss']
})
export class TextListPresenterComponent implements OnInit {
  @Input()
  currentQuestion: QuestionSessionViewModel;
  @Output()
  answerEvent: EventEmitter<string> = new EventEmitter<string>();
  answersOptions: string[];
  answers: string[] = [];
  constructor(private listPresenterService: ListPresenterService) { }

  ngOnInit(): void {
    this.answersOptions = this.listPresenterService.getAnswers(this.currentQuestion);
  }
  done() {
    this.answerEvent.emit(JSON.stringify(this.answers));
  }
}
