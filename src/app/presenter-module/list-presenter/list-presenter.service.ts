import { Injectable } from '@angular/core';
import { QuestionSessionViewModel } from 'src/app/api-client';

@Injectable({
  providedIn: 'root',
})
export class ListPresenterService {
  getAnswers(currentQuestion: QuestionSessionViewModel): string[] {
    let answersOptions = [];
    for (let index = 0; index < currentQuestion.answers.length; index++) {
      if (currentQuestion.answers[index].value == null) {
        continue;
      }
      let elements;
      try {
        elements = JSON.parse(currentQuestion.answers[index].value);
      } catch (error) {
        elements = currentQuestion.answers[index].value;
      }
      answersOptions = answersOptions.concat(elements);
    }
    return answersOptions;
  }

  constructor() {}
}
