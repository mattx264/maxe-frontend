import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { QuestionSectionViewModel, QuestionSessionViewModel } from 'src/app/api-client';
import { QuestionSessionViewModelQA } from 'src/app/test-common/question-session-view-models';

import { ListPresenterComponent } from './list-presenter.component';

describe('ListPresenterComponent', () => {
  let component: ListPresenterComponent;
  let fixture: ComponentFixture<ListPresenterComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ListPresenterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListPresenterComponent);
    component = fixture.componentInstance;
    component.currentQuestion = QuestionSessionViewModelQA.getList()[0];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
