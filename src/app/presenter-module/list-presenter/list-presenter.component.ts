import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AnswerType, ContentSectionAnswerType, QuestionSessionViewModel } from 'src/app/api-client';

@Component({
  selector: 'app-list-presenter',
  templateUrl: './list-presenter.component.html',
  styleUrls: ['./list-presenter.component.scss'],
})
export class ListPresenterComponent implements OnInit {
  @Input()
  currentQuestion: QuestionSessionViewModel;
  @Output()
  answerEvent: EventEmitter<string> = new EventEmitter<string>();
  AnswerType = AnswerType;

  constructor() {}

  ngOnInit(): void {}
  childAnswerChange(event) {
    this.answerEvent.emit(event);
  }
}
