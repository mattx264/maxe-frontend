import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { QuestionSessionViewModel } from 'src/app/api-client';
import { ListPresenterService } from '../list-presenter.service';

@Component({
  selector: 'app-dnd-list-presenter',
  templateUrl: './dnd-list-presenter.component.html',
  styleUrls: ['./dnd-list-presenter.component.scss']
})
export class DndListPresenterComponent implements OnInit {
  @Input()
  currentQuestion: QuestionSessionViewModel;
  @Output()
  answerEvent: EventEmitter<string> = new EventEmitter<string>();
  answersOptions: string[];
  answers: string[] = [];
  constructor(private listPresenterService: ListPresenterService) { }

  ngOnInit(): void {
    this.answersOptions = this.listPresenterService.getAnswers(this.currentQuestion);
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
  }
  done() {
    this.answerEvent.emit(JSON.stringify(this.answers));
  }
}
