import { ComponentFixture, TestBed } from '@angular/core/testing';
import { QuestionSectionViewModel } from 'src/app/api-client';
import { QuestionSessionViewModelQA } from 'src/app/test-common/question-session-view-models';

import { DndListPresenterComponent } from './dnd-list-presenter.component';

describe('DndListPresenterComponent', () => {
  let component: DndListPresenterComponent;
  let fixture: ComponentFixture<DndListPresenterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DndListPresenterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DndListPresenterComponent);
    component = fixture.componentInstance;
    component.currentQuestion = QuestionSessionViewModelQA.getList()[0];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
