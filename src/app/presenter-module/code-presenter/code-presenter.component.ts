/// <reference path="../../../../node_modules/monaco-editor/monaco.d.ts" />


import { Component, OnInit, Input, Output, EventEmitter, forwardRef, ViewChild, ElementRef, NgZone } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { BaseEditor } from './base-editor';
import { CodeEditorModel } from './types';

@Component({
  selector: 'app-code-presenter',
  templateUrl: './code-presenter.component.html',
  styleUrls: ['./code-presenter.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => CodePresenterComponent),
    multi: true
  }]
})
export class CodePresenterComponent extends BaseEditor implements OnInit {
  @Input()
  type: string;
  @Input()
  value: string;
  @Input()
  isReadOnly: boolean;

  @Output()
  answerEvent: EventEmitter<string> = new EventEmitter<string>();
  @ViewChild('editorContainer', { static: true }) _editorContainer: ElementRef;

  protected _editor: any;
  private _value: any = '';

  // options = {
  //   model: {},
  // };
  constructor(private zone: NgZone) {
    super()
  }

  ngOnInit(): void {
    let val = '';
    if (this.value != null) {
      val = this.value;
    } else {

    }
    let codeModelTemplate: CodeEditorModel = {
      uri: 'inmemory://template' + this.isReadOnly,
      language: this.type,
      value: val,
      readOnly: true
    }
    this.options = {};
    this.options.model = codeModelTemplate;
    // this.initMonaco(this.options);
  }
  protected initMonaco(options: any): void {

    const hasModel = !!options.model;

    if (hasModel) {
      const model = monaco.editor.getModel(options.model.uri || '');
      if (model) {
        options.model = model;
        options.model.setValue(this._value);
        // options.model.setModelLanguage(model, "javascript")
        // options.model.set
      } else {
        options.model = monaco.editor.createModel(options.model.value, options.model.language, options.model.uri);
        options.readonly = true;
        options.model.readonly = true;
      }
    }

    this._editor = monaco.editor.create(this._editorContainer.nativeElement, options);
    this._editor.readOnly = true;

    if (!hasModel) {
      this._editor.setValue(this._value);
    }

    this._editor.onDidChangeModelContent((e: any) => {
      const value = this._editor.getValue();

      this.zone.run(() => this._value = value);
    });

    this._editor.onDidBlurEditorWidget(() => {

    });

    // refresh layout on resize event.
    // if (this._windowResizeSubscription) {
    //   this._windowResizeSubscription.unsubscribe();
    // }
    // this._windowResizeSubscription = fromEvent(window, 'resize').subscribe(() => this._editor.layout());
    // this.onInit.emit(this._editor);
  }
  answerChange() {
    this.answerEvent.emit(this.value);
  }
}
