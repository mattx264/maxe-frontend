
export interface DiffCodeEditorModel {
    code: string;
    language: string;
}
export interface CodeEditorModel {
    value: string;
    language?: string;
    uri?: any;
    readOnly: boolean;
}