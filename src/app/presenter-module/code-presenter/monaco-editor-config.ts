

export interface MonacoEditorConfig {
  baseUrl?: string;
  defaultOptions?: { [key: string]: any; },
  onMonacoLoad?: Function;
}