import { AfterViewInit, ElementRef, EventEmitter, Input, OnDestroy, Output, ViewChild, Directive, Component } from '@angular/core';
import { Subscription } from 'rxjs';
import { MonacoEditorConfig } from './monaco-editor-config';

let loadedMonaco = false;
let loadPromise: Promise<void>;
declare const require: any;
@Component({
  template: ''
})
export abstract class BaseEditor implements AfterViewInit, OnDestroy {
  @ViewChild('editorContainer', {static: true}) _editorContainer: ElementRef;
  @Output() onInit = new EventEmitter<any>();
  protected _editor: any;
  private _options: any;
  protected _windowResizeSubscription: Subscription;

  @Input('options')
  set options(options: any) {
    this._options = Object.assign({}, this.config.defaultOptions, options);
    if (this._editor) {
      this._editor.dispose();
      this.initMonaco(options);
    }
  }

  get options(): any {
    return this._options;
  }
  config: MonacoEditorConfig = {
    baseUrl: '/libs/vs', // configure base path for monaco edito    
    defaultOptions: { scrollBeyondLastLine: false },
    onMonacoLoad: () => {
      console.log((window as any).monaco);
    }
  }
  constructor() {

  }

  ngAfterViewInit(): void {
    if(this.options ==undefined){
      this.options = this.config.defaultOptions;
    }
    if (loadedMonaco) {
      // Wait until monaco editor is available
      loadPromise.then(() => {
        this.initMonaco(this.options);
      });
    } else {
      loadedMonaco = true;
      loadPromise = new Promise<void>((resolve: any) => {
        const baseUrl = this.config.baseUrl || '/libs/vs';
        if (typeof ((<any>window).monaco) === 'object') {
          resolve();
          return;
        }
        const onGotAmdLoader: any = () => {
          // Load monaco
          (<any>window).require.config({ paths: { 'vs': `${baseUrl}` } });
          (<any>window).require(['vs/editor/editor.main'], () => {
            if (typeof this.config.onMonacoLoad === 'function') {
              this.config.onMonacoLoad();
            }
            this.initMonaco(this.options);
            resolve();
          });
        };

        // Load AMD loader if necessary
        if (!(<any>window).require) {
          const loaderScript: HTMLScriptElement = document.createElement('script');
          loaderScript.type = 'text/javascript';
          loaderScript.src = `${baseUrl}/loader.js`;
          loaderScript.addEventListener('load', onGotAmdLoader);
          document.body.appendChild(loaderScript);
        } else {
          onGotAmdLoader();
        }
      });
    }
  }

  protected abstract initMonaco(options: any): void;

  ngOnDestroy() {
    if (this._windowResizeSubscription) {
      this._windowResizeSubscription.unsubscribe();
    }
    if (this._editor) {
      this._editor.dispose();
      this._editor = undefined;
    }
  }
}