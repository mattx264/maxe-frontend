import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SoundPresenterComponent } from './sound-presenter.component';

describe('SoundPresenterComponent', () => {
  let component: SoundPresenterComponent;
  let fixture: ComponentFixture<SoundPresenterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SoundPresenterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SoundPresenterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
