import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-sound-presenter',
  templateUrl: './sound-presenter.component.html',
  styleUrls: ['./sound-presenter.component.scss']
})
export class SoundPresenterComponent implements OnInit {
  @Input()
  soundPath: string;
  constructor() { }

  ngOnInit(): void {
  }

}
