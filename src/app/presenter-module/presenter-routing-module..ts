import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FlexiblePresenterComponent } from './flexible-presenter/flexible-presenter.component';
import { ScreenCalibrateComponent } from './flexible-presenter/screen-calibrate/screen-calibrate.component';
import { CanDeactivateGuard } from './flexible-presenter/can-deactivate.guard';
import { ResultPresenterComponent } from './result-presenter/result-presenter.component';
import { SearchExamComponent } from './search-exam/search-exam.component';
import { StartupExamPresenterComponent } from './startup-exam-presenter/startup-exam-presenter.component';
import { InteractivePresenterComponent } from './interactive-presenter/interactive-presenter.component';
import { QuickKnowledgePresenterComponent } from './quick-knowledge-presenter/quick-knowledge-presenter.component';
import { QuickKnowledgePresenterResolverService } from './quick-knowledge-presenter/quick-knowledge-presenter-resolver.service';
const routes: Routes = [
  {
    path: '',
    redirectTo: 'search',
    pathMatch: 'full',
  },
  {
    path: 'search',
    component: SearchExamComponent,
    canDeactivate: [CanDeactivateGuard],
  },
  {
    path: 'startup/:title/:id',
    component: StartupExamPresenterComponent,
    canDeactivate: [CanDeactivateGuard],
  },
  {
    path: 'flexible/:title/:learnRunGuid',
    component: FlexiblePresenterComponent,
    canDeactivate: [CanDeactivateGuard],
  },
  {
    path: 'interactive/:title/:learnRunGuid',
    component: InteractivePresenterComponent,
  },
  {
    path: 'result/:guid',
    component: ResultPresenterComponent,
    canDeactivate: [CanDeactivateGuard],
  },
  {
    path: 'calibrate',
    component: ScreenCalibrateComponent,
  },
  {
    path: 'quick-knowledge/:title/:quickKnowledgeRunGuid',
    component: QuickKnowledgePresenterComponent,
    resolve: { loadData: QuickKnowledgePresenterResolverService },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PresenterRoutingModule {}
