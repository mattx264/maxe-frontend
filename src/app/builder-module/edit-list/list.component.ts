import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AuthService } from '../../auth/services/auth.service';
import * as actions from '../state/builder.actions';
import * as selectors from '../state/builder.selectors';
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
  // tests: Test[] = [];
  exams$ = this.store.select(selectors.selectExamSearch);
  constructor(
    private authService: AuthService,
    private store: Store // private httpClient: HttpClientService
  ) {
    this.store.dispatch(actions.getUserExams());
  }

  ngOnInit() {
    // this.httpClient.get('test').subscribe((response: any[]) => {
    //   this.tests = response;
    // });
    //this.db.collection('tests', ref => ref.where('userId', '==', this.authService.getCurrentUser().uid)).valueChanges().subscribe((data: Test[]) => {
    //   this.tests = data;
    // });
  }
}
