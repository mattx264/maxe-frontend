import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LandingPageCreateComponent } from './landing-page-create.component';

describe('LandingPageCreateComponent', () => {
  let component: LandingPageCreateComponent;
  let fixture: ComponentFixture<LandingPageCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LandingPageCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LandingPageCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
