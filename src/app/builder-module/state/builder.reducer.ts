import { Action, createReducer, on } from '@ngrx/store';

import {
  BuilderExamViewModel,
  ExamSearchViewModel,
  QuestionModelBackUp,
} from 'src/app/api-client';

import * as actions from './builder.actions';

export const BUILDER_STATE_FEATURE_KEY = 'builder state';

export interface BuilderState {
  examSearchViewModel: ExamSearchViewModel[] | undefined;
  builderExamViewModel: BuilderExamViewModel | undefined;
}

const initState: BuilderState = {
  examSearchViewModel: undefined,
  builderExamViewModel: undefined,
};

const builderReducer = createReducer(
  initState,
  on(
    actions.getExamSuccessful,
    actions.updateBuilderExamViewModel,
    (state, { builderExamViewModel }) => ({
      ...state,
      builderExamViewModel,
    })
  ),
  on(actions.getUserExamsSuccessful, (state, { examSearchViewModel }) => ({
    ...state,
    examSearchViewModel,
  }))
);

export function reducer(state: BuilderState | undefined, action: Action) {
  return builderReducer(state, action);
}
