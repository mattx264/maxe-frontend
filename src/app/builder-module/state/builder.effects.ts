import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import * as actions from './builder.actions';
import * as selectors from './builder.selectors';
import { switchMap, catchError, map, withLatestFrom } from 'rxjs/operators';
import { of } from 'rxjs';
import { Store } from '@ngrx/store';
import { BuilderSearchClient, ExamsBuilderClient } from 'src/app/api-client';
@Injectable()
export class BuilderEffects {
  constructor(
    private readonly actions$: Actions,
    private readonly builderSearchClient: BuilderSearchClient,
    private readonly ExamsBuilderClient: ExamsBuilderClient,
    private readonly store: Store
  ) {}
  getUserExams$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.getUserExams),
        switchMap(() =>
          this.builderSearchClient
            .getByUser()
            .pipe(
              map((result) =>
                actions.getUserExamsSuccessful({ examSearchViewModel: result })
              )
            )
        )
      )

    // postActivity$ = createEffect(() =>
    //   this.actions$.pipe(
    //     ofType(actions.postActivity),
    //     withLatestFrom(this.store.select(selectors.activeClass)),
    //     switchMap(([, activeClass]) =>
    //       this.unifiedPortalApis
    //         .rootApi_PostActivity(activeClass?.ClassId!, {
    //           ClassId: activeClass?.ClassId!,
    //           AdClassId: activeClass?.AdClassId,
    //         })
    //         .pipe(
    //           map((activityResult) =>
    //             actions.postActivitySuccess({
    //               activityResult: activityResult?.Data!,
    //             })
    //           ),
    //           catchError(() => of(actions.postActivityError()))
    //         )
    //     )
    //   )
    // );
  );
  getExam$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.getExam),
      switchMap((action) =>
        this.ExamsBuilderClient.get(action.examId).pipe(
          map((builderExamViewModel) =>
            actions.getExamSuccessful({ builderExamViewModel })
          )
        )
      )
    )
  );
}
