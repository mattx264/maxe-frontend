import {
  BuilderState,
  BUILDER_STATE_FEATURE_KEY as BUILDER_STATE_FEATURE_KEY,
} from './builder.reducer';
import { createSelector, createFeatureSelector } from '@ngrx/store';

const builderFeature = createFeatureSelector<BuilderState>(
  BUILDER_STATE_FEATURE_KEY
);

export const selectExamSearch = createSelector(
  builderFeature,
  (state) => state.examSearchViewModel
);

export const selectSetupForm = createSelector(
  builderFeature,
  (state) => state.builderExamViewModel
);
