import { createAction, props } from '@ngrx/store';
import { BuilderExamViewModel, ExamSearchViewModel } from 'src/app/api-client';

export const getUserExams = createAction(
  '[Effect get user exams] call backend to get list of exams'
);
export const getUserExamsSuccessful = createAction(
  '[API return] call backend to get list of exams successful',
  props<{ examSearchViewModel: ExamSearchViewModel[] }>()
);
export const getExam = createAction(
  '[Effect get exam] call backend to pull details exam info',
  props<{ examId: number }>()
);
export const getExamSuccessful = createAction(
  '[API return] get exam info successful',
  props<{ builderExamViewModel: BuilderExamViewModel }>()
);
export const updateBuilderExamViewModel = createAction(
  '[Reducer update] update builder model after create or update',
  props<{ builderExamViewModel: BuilderExamViewModel }>()
);

// export const caching = createAction('[Effecy Caching]');
// export const setAnnouncementCount = createAction(
//     '[Class] set announcement count',
//     props<{ announcements: SideBarInformationStore[] }>(),
// );
