import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LandingPageCreateComponent } from './landing-page-create/landing-page-create.component';
import { MaterialModule } from '../shared-module/material-module';
import { RouterModule } from '@angular/router';
import { BuilderRoutingModule } from './builder-routing.module';
import { ImportExamComponent } from './import-exam/import-exam.component';
import {
  BuilderSearchClient,
  CategoryExamClient,
  ExamsBuilderClient,
  FileClient,
  FileExamBuilderClient,
} from '../api-client';
import { SharedModule } from '../shared-module/shared-module.module';
import {
  ConnectFormDirective,
  SetupPageComponent,
} from './setup-page/setup-page.component';
import { ListComponent } from './edit-list/list.component';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { BUILDER_STATE_FEATURE_KEY, reducer } from './state/builder.reducer';
import { BuilderEffects } from './state/builder.effects';
import { EditComponent } from './edit/edit.component';
import { QuestionWrapperComponent } from './question-wrapper/question-wrapper.component';
import { QuizCreateComponent } from './add/quiz-create/quiz-create.component';
import { FilesLibraryComponent } from './shared/files-library/files-library.component';
import { MAT_LEGACY_SNACK_BAR_DEFAULT_OPTIONS as MAT_SNACK_BAR_DEFAULT_OPTIONS } from '@angular/material/legacy-snack-bar';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BuilderRoutingModule,
    MaterialModule,
    RouterModule,
    SharedModule,
    StoreModule.forFeature(BUILDER_STATE_FEATURE_KEY, reducer),
    EffectsModule.forFeature([BuilderEffects]),
  ],
  declarations: [
    LandingPageCreateComponent,
    ImportExamComponent,
    SetupPageComponent,
    EditComponent,
    ListComponent,
    QuestionWrapperComponent,
    QuizCreateComponent,
    FilesLibraryComponent,
    ConnectFormDirective,
  ],
  providers: [
    FileExamBuilderClient,
    FileClient,
    BuilderSearchClient,
    ExamsBuilderClient,
    CategoryExamClient,
    { provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: { duration: 2500 } },
  ],
})
export class BuilderModule {}
