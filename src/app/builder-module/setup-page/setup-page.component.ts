import { Component, Directive, Input, OnDestroy, OnInit } from '@angular/core';
import {
  Validators,
  UntypedFormBuilder,
  UntypedFormGroup,
  FormGroupDirective,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as actions from '../state/builder.actions';
import * as selectors from '../state/builder.selectors';
import {
  BuilderExamViewModel,
  CategoryExamClient,
  CategoryExamViewModel,
  ExamModelStatusEnum,
  ExamsBuilderClient,
} from 'src/app/api-client';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-setup-page',
  templateUrl: './setup-page.component.html',
  styleUrls: ['./setup-page.component.scss'],
})
export class SetupPageComponent implements OnInit, OnDestroy {
  formGroup = this.fb.group({
    title: '',
    description: '',
    categoryExamId: 0,
    fileModelId: 0,
  });
  form$ = this.store.select(selectors.selectSetupForm);
  categoryExams: CategoryExamViewModel[];
  examId: number;
  sub: any;

  constructor(
    private fb: UntypedFormBuilder,
    private router: Router,
    private categoryExamClient: CategoryExamClient,
    private examsBuilderClient: ExamsBuilderClient,
    private activatedRoute: ActivatedRoute,
    private store: Store
  ) {}

  async ngOnInit(): Promise<void> {
    this.categoryExams = await this.categoryExamClient.get().toPromise();

    this.formGroup = this.buildForm();
    this.sub = this.activatedRoute.params.subscribe((params) => {
      this.examId = params.examId;
      if (this.examId != undefined) {
        this.examId = +this.examId;
        this.store.dispatch(actions.getExam({ examId: this.examId }));
      } else {
        this.store.dispatch(
          actions.getExamSuccessful({
            builderExamViewModel: new BuilderExamViewModel({
              id: 0,
              categoryExamId: 0,
              title: '',
              userModelId: 0,
              description: '',
              fileModelId: 0,
              status: ExamModelStatusEnum.Draft,
              isActive: true,
              dateAdded: new Date(),
              dateModified: new Date(),
              questionModels: [],
            }),
          })
        );
      }
    });
  }
  buildForm(): UntypedFormGroup {
    return this.fb.group({
      title: [null, Validators.required],
      description: [null, Validators.required],
      isPublic: [true],
      categoryExamId: [null, Validators.required],
      fileModelId: [null, Validators.required],
    });
  }

  async setupClicked(examId: number): Promise<void> {
    if (this.formGroup.invalid) {
      return;
    }
    const builderExamViewModel: BuilderExamViewModel =
      new BuilderExamViewModel();
    builderExamViewModel.categoryExamId =
      +this.formGroup.get('categoryExamId').value;
    builderExamViewModel.title = this.formGroup.get('title').value;
    builderExamViewModel.description = this.formGroup.get('description').value;
    builderExamViewModel.language = 'en';
    builderExamViewModel.fileModelId = this.formGroup.get('fileModelId').value;
    builderExamViewModel.status = ExamModelStatusEnum.Draft;
    if (!examId || examId === 0) {
      builderExamViewModel.id = await this.examsBuilderClient
        .post(builderExamViewModel)
        .toPromise();
    } else {
      builderExamViewModel.id = examId;
      await this.examsBuilderClient.put(builderExamViewModel).toPromise();
    }
    this.store.dispatch(
      actions.updateBuilderExamViewModel({
        // we have to set builderExamViewModel to null to prevent from double call in edit component
        // because we will render edit component - builderExamViewModel is there then
        // we will get builderExamViewModel from backend and all that was set is lost
        builderExamViewModel: null,
      })
    );
    setTimeout(() => {
      this.router.navigate(['/builder/question', builderExamViewModel.id, 1]);
    }, 1000);
  }
  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }
}
@Directive({ selector: '[connectForm]' })
export class ConnectFormDirective {
  @Input('connectForm')
  set data(val: any) {
    if (val) {
      this.formGroupDirective.form.patchValue(val);
      this.formGroupDirective.form.markAsPristine();
    }
  }
  constructor(private formGroupDirective: FormGroupDirective) {}
}
