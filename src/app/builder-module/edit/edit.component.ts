import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { AuthService } from 'src/app/auth/services/auth.service';
import { UntypedFormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import * as actions from '../state/builder.actions';
import * as selectors from '../state/builder.selectors';
import { HttpClient } from '@angular/common/http';
import { BuilderExamViewModel, ExamsBuilderClient } from 'src/app/api-client';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
})
export class EditComponent implements OnInit, OnDestroy {
  sub: any;
  questionIndex: number;
  quizType: any;
  questionModel: any;
  form$ = this.store.select(selectors.selectSetupForm);

  reviewShow = false;
  // EDIT VAR
  examId: number;
  // END EDIT VAR
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private store: Store,
    private authService: AuthService,
    private fb: UntypedFormBuilder,
    private examsBuilderClient: ExamsBuilderClient
  ) {}
  ngOnInit(): void {
    this.sub = this.activatedRoute.params.subscribe((params) => {
      this.examId = params['examId'];
      if (this.examId == undefined) {
        this.router.navigate(['/create/list']);
      }
      this.store.dispatch(actions.getExam({ examId: +this.examId }));
    });
  }
  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }
}
