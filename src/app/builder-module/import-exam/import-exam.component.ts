import {
  HttpClient,
  HttpEventType,
  HttpStatusCode,
} from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { MatLegacySnackBar as MatSnackBar } from '@angular/material/legacy-snack-bar';
import { Subscription } from 'rxjs';
import { FileClient, FileViewModel } from 'src/app/api-client';

@Component({
  selector: 'app-import-exam',
  templateUrl: './import-exam.component.html',
  styleUrls: ['./import-exam.component.scss'],
})
export class ImportExamComponent implements OnInit {
  fileName = '';
  uploadProgress: number;
  uploadSub: Subscription;
  message: string;
  uploadMessage: string;
  constructor(private snackBar: MatSnackBar) {}

  async ngOnInit(): Promise<void> {}

  uploadEvent(event) {
    if (event.status == HttpStatusCode.Ok) {
      this.message = 'Upload and processing file successful';
    }
  }
}
