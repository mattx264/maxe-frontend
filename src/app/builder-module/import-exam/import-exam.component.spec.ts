import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FileClient } from 'src/app/api-client';

import { ImportExamComponent } from './import-exam.component';

describe('ImportExamComponent', () => {
  let component: ImportExamComponent;
  let fixture: ComponentFixture<ImportExamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImportExamComponent ],
      imports:[HttpClientModule],
      providers:[FileClient]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportExamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
