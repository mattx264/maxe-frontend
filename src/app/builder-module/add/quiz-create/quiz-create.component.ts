import { Component, OnInit } from '@angular/core';
import { CreateComponentBase } from '../createComponentBase';
import { UntypedFormGroup, UntypedFormBuilder, Validators, UntypedFormArray } from '@angular/forms';

@Component({
  selector: 'app-quiz-create',
  templateUrl: './quiz-create.component.html',
  styleUrls: ['./quiz-create.component.scss'],
})
export class QuizCreateComponent extends CreateComponentBase implements OnInit {
  get answers(): any {
    return this.formGroup.get('answers');
  }
  private _model: UntypedFormGroup;
  public get model(): UntypedFormGroup {
    return this._model;
  }
  public set model(v: UntypedFormGroup) {
    this._model = v;

    if (this.model !== undefined) {
      if (this.model instanceof UntypedFormGroup) {
        this.formGroup = this._model;
      } else {
        this.formGroup = this.buildForm();
      }
    }
  }
  constructor(protected fb: UntypedFormBuilder) {
    super(fb);
  }

  ngOnInit() {
    if (this.editData != null) {
      this.updateEditDate();
      //  this.formGroup.controls.answers=this.fb.array([this.createAnswer(), this.createAnswer(), this.createAnswer(), this.createAnswer()]),
    }
    if (this.formGroup !== undefined && this.formGroup !== null) {
      return;
    }
    this.formGroup = this.buildForm();
    //if this is new/create
    //add 4 default options
  }
  buildForm(): UntypedFormGroup {
    return this.fb.group({
      question: ['', Validators.required],
      answers: this.fb.array([
        this.createAnswer(),
        this.createAnswer(),
        this.createAnswer(),
        this.createAnswer(),
      ]),
      makeQuestionHard: false,
    });
  }
  updateEditDate() {
    this.formGroup = this.buildForm();
    if (this.editData == null) {
      return;
    }
    if (this.editData.questionText != null) {
      this.formGroup.controls.question.setValue(this.editData.questionText);
    }
    if (this.editData.answer != null) {
      let answers = JSON.parse(this.editData.answer);

      this.formGroup.controls.answers = this.fb.array([]);
      for (let i = 0; i < answers.length; i++) {
        const element = answers[i];
        (this.formGroup.controls.answers as UntypedFormArray).push(
          this.createAnswer(element.value, element.isCorrect)
        );
      }
    }
  }
  createAnswer(value: string = '', isCorrect: boolean = false) {
    return this.fb.group({
      value: [value],
      isCorrect: [isCorrect],
    });
  }
  removeAnswer(index: number) {
    (this.formGroup.controls.answers as UntypedFormArray).removeAt(index);
  }
  submitClicked(): UntypedFormGroup {
    this.formGroup.markAsTouched();
    if (this.formGroup.invalid) {
      return undefined;
    }
    return this.formGroup;
  }
}
