import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColumnDADCreateComponent } from './column-dadcreate.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MaterialModule } from '../../../shared/material-module';
import { SharedModule } from '../../../shared/shared.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

describe('ColumnDADCreateComponent', () => {
  let component: ColumnDADCreateComponent;
  let fixture: ComponentFixture<ColumnDADCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColumnDADCreateComponent ],
      imports:[ReactiveFormsModule,FormsModule,MaterialModule,SharedModule,NoopAnimationsModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColumnDADCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
