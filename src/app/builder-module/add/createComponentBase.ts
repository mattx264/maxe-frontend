import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { Output, EventEmitter, Input, Directive } from '@angular/core';

@Directive()
export abstract class CreateComponentBase {
  formGroup: UntypedFormGroup;
  @Input() abstract model: UntypedFormGroup;

  private _editData: any;
  @Input()
  public get editData(): any {
    return this._editData;
  }
  public set editData(v: any) {
    this._editData = v;
    this.updateEditDate();
  }

  constructor(protected fb: UntypedFormBuilder) {}
  abstract buildForm(): UntypedFormGroup;
  abstract submitClicked(): UntypedFormGroup;
  abstract updateEditDate();
}
