import { HttpStatusCode } from '@angular/common/http';
import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
  ChangeDetectorRef,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { MatLegacyDialog as MatDialog } from '@angular/material/legacy-dialog';
import { MatLegacySnackBar as MatSnackBar } from '@angular/material/legacy-snack-bar';
import { FileClient, FileViewModel } from 'src/app/api-client';
import { ConfirmationModalComponent } from 'src/app/shared-module/componets/confirmation-modal/confirmation-modal.component';

@Component({
  selector: 'app-files-library',
  templateUrl: './files-library.component.html',
  styleUrls: ['./files-library.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: FilesLibraryComponent,
    },
  ],
})
export class FilesLibraryComponent implements ControlValueAccessor, OnInit {
  @Input()
  requiredFileType = '.csv';
  @Input()
  isSelectable = false;
  uploadMessage: string;
  files: FileViewModel[] | null = null;
  selectedFile: FileViewModel | null = null;
  onChange = (quantity) => {};

  onTouched = () => {};
  touched = false;

  disabled = false;
  fileId: number;
  constructor(
    private fileClient: FileClient,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private cdr: ChangeDetectorRef
  ) {}

  async ngOnInit(): Promise<void> {
    await this.getImages();
  }
  removeClick(img): void {
    const dialogRef = this.dialog.open(ConfirmationModalComponent);
    dialogRef.componentInstance.body = 'Do you want to remove file?';
    dialogRef.afterClosed().subscribe(async (result) => {
      if (result === true) {
        await this.fileClient.delete(img.id).toPromise();
        await this.getImages();
      }
    });
  }
  async getImages(): Promise<void> {
    this.files = await this.fileClient.getFiles().toPromise();
    if (this.fileId) {
      this.selectFile();
    }

    this.cdr.markForCheck();
  }
  writeValue(fileId: number): void {
    if (!fileId || fileId == 0) {
      if (this.selectedFile != null) {
        this.selectedFile = null;
      }
      this.fileId = fileId;
      return;
    }
    this.fileId = fileId;
    if (this.files.length === 0 && fileId > 0) {
      return;
    }

    this.selectFile();
  }
  private selectFile() {
    const file = this.files.find((x) => x.id === this.fileId);
    if (file == null) {
      throw new Error(
        'File in not found in collection, file id:' + this.fileId
      );
    }
    this.selectedFile = file;
  }
  registerOnChange(onChange: any): void {
    this.onChange = onChange;
  }

  registerOnTouched(onTouched: any): void {
    this.onTouched = onTouched;
  }

  markAsTouched(): void {
    if (!this.touched) {
      this.onTouched();
      this.touched = true;
    }
  }

  setDisabledState(disabled: boolean): void {
    this.disabled = disabled;
  }

  onImgClick(file: FileViewModel) {
    this.selectedFile = file;
    this.onChange(this.selectedFile.id);
  }
  copyToClipboardClick(fileName: string) {
    const copyTextarea = document.querySelector(
      '.js-copytextarea'
    ) as HTMLInputElement;
    copyTextarea.style.visibility = 'visible';
    copyTextarea.value = fileName;
    copyTextarea.focus();
    copyTextarea.select();
    try {
      const successful = document.execCommand('copy');
      copyTextarea.style.visibility = 'hidden';
      this.snackBar.open('Guid copied to clipboard');
    } catch (err) {
      this.snackBar.open('Not able to copy to clipboard');
    }
  }
  uploadFileEvent(event) {
    if (event.status == HttpStatusCode.Ok) {
      this.files.push(event.body);
      this.uploadMessage = 'Upload file successful';
      this.cdr.markForCheck();
    }
  }
}
