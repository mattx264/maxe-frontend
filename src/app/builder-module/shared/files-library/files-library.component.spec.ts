import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilesLibraryComponent } from './files-library.component';

describe('FilesLibraryComponent', () => {
  let component: FilesLibraryComponent;
  let fixture: ComponentFixture<FilesLibraryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FilesLibraryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FilesLibraryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
