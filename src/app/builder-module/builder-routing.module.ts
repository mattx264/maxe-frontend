import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LandingPageCreateComponent } from './landing-page-create/landing-page-create.component';
import { ImportExamComponent } from './import-exam/import-exam.component';
import { SetupPageComponent } from './setup-page/setup-page.component';
import { ListComponent } from './edit-list/list.component';
import { QuestionWrapperComponent } from './question-wrapper/question-wrapper.component';
import { EditComponent } from './edit/edit.component';

const routes: Routes = [
  {
    path: '',
    component: LandingPageCreateComponent,
  },
  {
    path: 'setup',
    component: SetupPageComponent,
  },
  {
    path: 'question/:examId',
    component: SetupPageComponent,
  },
  {
    path: 'question/:examId/:questionIndex',
    component: EditComponent,
  },
  {
    path: 'list',
    component: ListComponent,
  },
  {
    path: 'import',
    component: ImportExamComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BuilderRoutingModule {}
