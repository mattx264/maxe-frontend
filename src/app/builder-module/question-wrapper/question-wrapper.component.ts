import {
  Component,
  OnInit,
  ViewChild,
  Input,
  EventEmitter,
  Output,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { UntypedFormBuilder, FormGroup } from '@angular/forms';
import { MatAccordion } from '@angular/material/expansion';
import { MatLegacySnackBar as MatSnackBar } from '@angular/material/legacy-snack-bar';

import { ActivatedRoute, Router } from '@angular/router';
import {
  AnswerModelBackup,
  AnswerType,
  ContentSectionAnswer,
  ContentSectionAnswerBackup,
  ContentSectionAnswerType,
  ContentSectionCorrectAnswerBackup,
  ContentSectionQuestionBackup,
  ContentSectionQuestionType,
  DifficultyLevel,
  ExamsBuilderClient,
  IQuestionModelBackUp,
  QuestionModelBackUp,
} from 'src/app/api-client';
import { CreateComponentBase } from '../add/createComponentBase';

@Component({
  selector: 'app-question-wrapper',
  templateUrl: './question-wrapper.component.html',
  styleUrls: ['./question-wrapper.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class QuestionWrapperComponent implements OnInit {
  @ViewChild(MatAccordion) accordion: MatAccordion;

  ContentSectionQuestionType = ContentSectionQuestionType;
  AnswerType = AnswerType;
  ContentSectionAnswerType = ContentSectionAnswerType;
  DifficultyLevel = DifficultyLevel;
  selectedDifficultyLevel: DifficultyLevel;
  answerList?: AnswerModelBackup;
  questionIndex = 0;

  createComponentBase: CreateComponentBase;

  private _questions: IQuestionModelBackUp[];
  @Input()
  public get questions(): IQuestionModelBackUp[] {
    return this._questions;
  }
  public set questions(v: IQuestionModelBackUp[]) {
    this._questions = v;
  }

  // @Input() questions: IQuestionModelBackUp[];
  @Input() examModelId: number;

  questionModel: IQuestionModelBackUp;
  isQuickModeOn = false;

  @ViewChild('quiz') set quiz(content: CreateComponentBase) {
    this.createComponentBase = content;
  }
  // private scrollService: ScrollService,
  constructor(
    private route: Router,
    private router: ActivatedRoute, //  private questionService: QuestionService
    private examsBuilderClient: ExamsBuilderClient,
    private fb: UntypedFormBuilder,
    private cdr: ChangeDetectorRef,
    private matSnackBar: MatSnackBar
  ) {
    this.isQuickModeOn = JSON.parse(localStorage.getItem('builder-mode'));
  }

  ngOnInit(): void {
    if (this.questions.length === 0) {
      this.addNewQuestionClick();
    }
    setTimeout(() => {
      this.setQuestion(0);
    });
  }

  setQuestion(index: number): void {
    //  this.route.navigate(['../' + (+index + 1)], { relativeTo: this.router });
    this.questionIndex = index;
    if (this.questions[index] == null) {
      this.setQuestion(this.questions.length - 1);

      this.cdr.markForCheck();
      return;
    }

    this.questionModel = JSON.parse(JSON.stringify(this.questions[index]));
    if (this.questionModel.answerList.length > 0) {
      this.selectedDifficultyLevel =
        this.questionModel.answerList[0].difficultyLevel;
      this.answerList = this.questionModel.answerList[0];
      if (this.isQuickModeOn) {
        // check if data is compatible with quick quiz mode
        if (
          this.questionModel.contentSectionCorrectAnswers[0]?.value &&
          this.questionModel?.answerList[0]?.wrongAnswers[0]?.value &&
          this.questionModel?.answerList[0]?.wrongAnswers[1]?.value
        ) {
        } else {
          this.matSnackBar.open('Data is not compatible with quiz quick mode');
          this.isQuickModeOn = false;
        }
      }
    } else if (this.isQuickModeOn) {
      this.addQuestionSectionClick();
      this.addCorrectAnswersClick();
      this.addDifficultyLevel('Normal');
      this.answerList.answerType = AnswerType.QUIZ;
      this.addWrongAnswer();
      this.addWrongAnswer();
      this.addWrongAnswer();
      this.addDifficultyLevel('Hard');
      this.answerList.answerType = AnswerType.TEXT;
    } else {
      this.selectedDifficultyLevel = null;
      this.answerList = null;
    }
    this.cdr.markForCheck();
  }
  toggleBuilderTemplateMode(): void {
    this.isQuickModeOn = !this.isQuickModeOn;
    localStorage.setItem('builder-mode', this.isQuickModeOn.toString());
  }
  changeDifficultyLevel(answerModel: AnswerModelBackup): void {
    this.selectedDifficultyLevel = answerModel.difficultyLevel;
    this.answerList = answerModel;
  }
  addDifficultyLevel(levelName: string): void {
    const answerModel = new AnswerModelBackup();
    answerModel.difficultyLevel = DifficultyLevel[levelName];
    this.questionModel.answerList.push(answerModel);
    this.selectedDifficultyLevel = answerModel.difficultyLevel;
    this.answerList = answerModel;
  }
  cancelDifficultyLevel(): void {
    this.removeDifficultyLevel();
  }
  removeDifficultyLevel(): void {
    const index = this.questionModel.answerList.indexOf(this.answerList);
    this.questionModel.answerList.splice(index, 1);
    this.answerList = undefined;
  }
  addWrongAnswer(): void {
    if (this.answerList.wrongAnswers == null) {
      this.answerList.wrongAnswers = [];
    }
    const wrongAnswer = new ContentSectionAnswerBackup();
    wrongAnswer.type = ContentSectionAnswerType.TEXT;
    this.answerList.wrongAnswers.push(wrongAnswer);
  }
  removedWrongAnswer(index: number): void {
    this.answerList.wrongAnswers.splice(index, 1);
  }
  addQuestionSectionClick(): void {
    const contentSectionQuestion = new ContentSectionQuestionBackup();
    contentSectionQuestion.value = '';
    contentSectionQuestion.type = ContentSectionQuestionType.TEXT;
    this.questionModel.questionSections.push(contentSectionQuestion);
  }
  removeQuestionSectionClick(index: number): void {
    this.questionModel.questionSections.splice(index, 1);
  }
  addCorrectAnswersClick(): void {
    const contentSectionCorrect = new ContentSectionCorrectAnswerBackup();
    contentSectionCorrect.value = '';
    contentSectionCorrect.type = ContentSectionAnswerType.TEXT;
    this.questionModel.contentSectionCorrectAnswers.push(contentSectionCorrect);
  }
  removeCorrectAnswersClick(index: number): void {
    this.questionModel.contentSectionCorrectAnswers.splice(index, 1);
  }
  addNewQuestionClick(): void {
    const newQuestion: IQuestionModelBackUp = {
      // START HERE -  this.questions[0].examModelId is incorrect because we will not have any question after new quiz is created
      examModelId: this.examModelId,
      id: 0,
      isActive: true,
      answerList: [],
      contentSectionCorrectAnswers: [],
      questionSections: [],
      createdBy: 'null',
      modifiedBy: 'null',
      dateAdded: new Date(),
      dateModified: new Date(),
    };
    this.questions = [...this.questions, newQuestion];
    this.setQuestion(this.questions.length - 1);
  }
  saveClick(): void {
    this.examsBuilderClient
      .putQuestion(this.questionModel as QuestionModelBackUp)
      .toPromise()
      .then((res) => {
        this.questionModel.id = res;
        this.matSnackBar.open('Saved');
      });
  }
  isInArray(
    answerModel: AnswerModelBackup[],
    difficultyLevel: string
  ): boolean {
    if (
      answerModel.find(
        (x) => DifficultyLevel[x.difficultyLevel] === difficultyLevel
      )
    ) {
      return false;
    }
    return true;
  }
  removeQuestionClick(): void {
    if (this.questionModel.id > 0) {
      this.examsBuilderClient
        .delete(this.questionModel.id)
        .toPromise()
        .then(() => this.removeFromLocalArray());
    } else {
      this.removeFromLocalArray();
    }
  }
  private removeFromLocalArray(): void {
    this.questions = [
      // part of the array before the given item
      ...this.questions.slice(0, this.questionIndex - 1),

      // part of the array after the given item
      ...this.questions.slice(this.questionIndex - 1 + 1),
    ];
    if (this.questions.length === 0) {
      this.questionIndex = 0;
    } else {
      this.questionIndex =
        this.questionIndex === 0 ? this.questionIndex : this.questionIndex - 1;
    }
    this.cdr.markForCheck();
  }
}
