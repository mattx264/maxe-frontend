import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './auth/guard/auth.guard';
import { LoginComponent } from './auth/login/login.component';
import { RegistrationComponent } from './auth/registration/registration.component';
import { SettingsComponent } from './settings/settings.component';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () =>
      import('./presenter-module/presenter.module').then(
        (x) => x.PresenterModule
      ),
  },
  {
    path: 'trivia',
    loadChildren: () =>
      import('./our-trivia-module/our-trivia.module').then(
        (x) => x.OurTriviaModule
      ),
  },
  {
    path: 'builder',
    loadChildren: () =>
      import('./builder-module/builder.module').then((x) => x.BuilderModule),
    canActivate: [AuthGuard],
  },
  {
    path: 'settings',
    component: SettingsComponent,
  },
  { path: 'login', component: LoginComponent },
  { path: 'registration', component: RegistrationComponent },
  {
    path: '**',
    redirectTo: 'home',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {})],
  exports: [RouterModule],
})
export class AppRoutingModule {}
