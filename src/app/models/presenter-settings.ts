export interface PresenterSettings {
    settings: KeyValue<string, string>[];
}

export interface KeyValue<T, G> {
    key: T;
    value: G;
}