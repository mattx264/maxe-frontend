export interface SaveExamRunModel {
    examIds: number[];
    questionRepeat: number;
    randomizeQuestions: boolean;
    questionLimit: number;//how may qestion 
}