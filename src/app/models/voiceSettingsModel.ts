export interface VoiceSettingsModel {
    rate: number;
    pitch: number;
    voiceName: string;
}