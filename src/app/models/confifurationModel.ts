export interface ButtonConfigurationModel {
    name: string;
    x: number;
    y: number;
    action: Function;
}